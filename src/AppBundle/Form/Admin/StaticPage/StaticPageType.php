<?php

namespace AppBundle\Form\Admin\StaticPage;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class StaticPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('upperField', TextareaType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control tinymce',
                        'data-theme' => 'advanced',
                        'rows' => '12'
                    )
                ))
            ->add('lowerField', TextareaType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control tinymce',
                        'data-theme' => 'advanced',
                        'rows' => '12'
                    )
                ))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-block btn-primary'),
                'label' => 'Save Static Page',
            ))
        ;
    }
}