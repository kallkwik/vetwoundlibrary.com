<?php

namespace AppBundle\Controller\Account;


use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Order;
use Symfony\Component\HttpFoundation\Request;

/**
 * Order controller.
 *
 * @Route("/account/order")
 * @Security("has_role('ROLE_SUBSCRIBED_USER')")
 */
class OrderController extends Controller
{
    /**
     * Lists all Order entities.
     *
     * @Route("/", name="account_order_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $orders = $em->getRepository('AppBundle:Order')->findBy(
            array(
                'user' => $this->getUser(),
            )
        );

        return $this->render('account/order/index.html.twig',
            array(
                'orders' => $orders,
            )
        );
    }

    /**
     * Finds and displays a Order entity.
     *
     * @Route("/{id}", name="account_order_show")
     * @Method("GET")
     */
    public function showAction(Order $order)
    {
        return $this->render('account/order/show.html.twig', array(
            'order' => $order,
        ));
    }

    /**
     * @Route("/new/{slug}", name="account_order_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Product $product, Request $request)
    {
        $order = new Order();

        $form = $this->createForm('AppBundle\Form\Account\Order\OrderType',
            $order,
            array('product' => $product)
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /**
             * Send email to distributor and admin
             */

            $order->setProduct($product);
            $order->setUser($this->getUser());

            $em->persist($order);
            $em->flush();

            $this->addFlash('Success', 'Order has been placed.');

            return $this->redirectToRoute('account_index');
        }

        return $this->render('account/order/new.html.twig',
            array(
                'product' => $product,
                'order' => $order,
                'form' => $form->createView(),
            )
        );
    }
}
