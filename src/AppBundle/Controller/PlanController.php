<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/plans")
 */
class PlanController extends Controller
{
    /**
     * @Route("/", name="plan_index")
     */
    public function indexAction()
    {
        $plans = $this->getDoctrine()->getRepository('AppBundle:Plan')->findAll();
        return $this->render('plan/index.html.twig', array('plans' => $plans));
    }
}