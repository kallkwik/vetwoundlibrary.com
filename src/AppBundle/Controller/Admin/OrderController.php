<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Order;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/order")
 * @Security("has_role('ROLE_ADMIN')")
 */
class OrderController extends Controller
{
    /**
     * @Route("/", name="admin_order_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_order_index_paginated",
     *     requirements={"page": "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Order')->findAll();

        $paginator = $this->get('knp_paginator');
        $orders = $paginator->paginate($query, $page, Order::NUM_ITEMS);
        $orders->setUsedRoute('admin_order_index_paginated');

        return $this->render('admin/order/index.html.twig',
            array(
                'orders' => $orders,
            )
        );
    }

    /**
     * @Route("/{id}", name="admin_order_show")
     */
    public function showAction(Order $order, Request $request)
    {
        /*$form = $this->createForm(
            'AppBundle\Form\Admin\Order\OrderType',
            $order
        );
        $form->handleRequest();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($order);
            $em->flush();
        }*/

        return $this->render('admin/order/show.html.twig',
            array(
                'order' => $order,
            )
        );
    }
}