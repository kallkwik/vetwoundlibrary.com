<?php

namespace AppBundle\Form\Admin\CaseStudy;

use AppBundle\Entity\CaseStudy;
use AppBundle\Form\Type\ImageType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class CaseStudyType extends AbstractType
{
    private $authChecker;
    private $tokenStorage;

    public function __construct(AuthorizationChecker $authorizationChecker,
                                TokenStorage $tokenStorage
    ) {
        $this->authChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityTypeOptions = array(
            'class' => 'AppBundle:User',
            'attr' => array('class' => 'form-control'),
            'choice_label' => function ($user) {
                return $user->getFirstName() . ' ' . $user->getLastName();
            },
            'query_builder' => function (EntityRepository $er) {
                if ($this->authChecker->isGranted('ROLE_ADMIN')) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.firstName', 'ASC')
                        ->where('u.roles LIKE :angel')
                        ->orWhere('u.roles LIKE :expert')
                        ->setParameter('angel', '%"ROLE_ANGEL"%')
                        ->setParameter('expert', '%"ROLE_EXPERT"%');
                } else {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.firstName', 'ASC')
                        ->where('u.id LIKE :id')
                        ->setParameter('id',
                            $this->tokenStorage->getToken()->getUser()->getId()
                        );
                }
            },
        );

        if ($this->authChecker->isGranted('ROLE_ADMIN')) {
            $entityTypeOptions['placeholder'] = 'Unassigned';
            $entityTypeOptions['required'] = false;
            $entityTypeOptions['label'] = 'Assign to Expert/Angel';
            $entityTypeOptions['disabled'] = false;

            $builder
                ->add('image', ImageType::class, array(
                    'label' => false,
                    'required' => false,
                ))
                ->add('published', ChoiceType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'Not Published' => CaseStudy::NOT_VISIBLE,
                        'Subscribed' => CaseStudy::SUBSCRIBED_VISIBLE,
                        'Public' => CaseStudy::PUBLIC_VISIBLE
                    )
                ))
            ;
        } else {
            $entityTypeOptions['required'] = true;
            $entityTypeOptions['label'] = 'Assigned to';
            $entityTypeOptions['disabled'] = true;

            $builder
                ->add('published', ChoiceType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'Not Published' => CaseStudy::NOT_VISIBLE,
                        'Subscribed' => CaseStudy::SUBSCRIBED_VISIBLE,
                        'Public' => CaseStudy::PUBLIC_VISIBLE
                    ),
                    'disabled' => true
                ))
            ;
        }

        $builder
            ->add('assignedUser', EntityType::class, $entityTypeOptions)
            ->add('submit', SubmitType::class,
                array(
                    'attr' => array('class' => 'btn btn-block btn-primary'),
                    'label' => 'Save'
                )
            )
        ;
    }
}