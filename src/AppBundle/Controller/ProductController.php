<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="product_index", defaults={"page"=1})
     * @Route("/page/{page}", name="product_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Product')->findByListedManufacturers();

        $paginator = $this->get('knp_paginator');
        $products = $paginator->paginate($query, $page,
            Product::NUM_ITEMS);
        $products->setUsedRoute('admin_distributor_index_paginted');

        return $this->render('product/index.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * @Route("/category/{categorySlug}", name="product_category_index")
     */
    public function categoryIndexAction(Category $category)
    {
        return $this->render('product/index.html.twig', array(
            'products' => $category->getProducts()
        ));
    }

    /**
     * @Route("/{slug}", name="product_show")
     */
    public function showAction(Product $product)
    {
        return $this->render('product/show.html.twig', array(
            'product' => $product
        ));
    }
}