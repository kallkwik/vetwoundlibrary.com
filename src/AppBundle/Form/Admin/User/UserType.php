<?php

namespace AppBundle\Form\Admin\User;

use AppBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('lastName', TextType::class, array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('email', TextType::class, array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('image', ImageType::class, array('required' => false))
            ->add('roles', ChoiceType::class, array(
                'choices' => array(
                    'Expert' => 'ROLE_EXPERT',
                    'Angel' => 'ROLE_ANGEL',
                    'Admin' => 'ROLE_ADMIN',
                ),
                'choices_as_values' => true,
                'multiple' => true,
                'expanded' => true,
                'attr' => array('class' => 'checkbox')
            ))
            ->add('published', ChoiceType::class,
                array(
                    'label' => 'Status',
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'Published' => true,
                        'Unpublished' => false,
                    ),
                )
            )
            ->add('description', TextareaType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'rows' => 4
                )
            ))
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Save User',
                    'attr' => array(
                        'class' => 'btn btn-block btn-primary'
                    )
                )
            )
        ;
    }
}
