<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findByListedManufacturers()
    {
        return $this->_em->createQueryBuilder()
            ->select('p')
            ->from($this->_entityName, 'p')
            ->innerJoin('p.manufacturer', 'm')
            ->where('m.listProducts = :listProducts')
            ->setParameter('listProducts', true)
            ->getQuery()
            ->getResult()
        ;
    }
}