<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_EXPERT') or has_role('ROLE_ANGEL') or has_role('ROLE_USER') or has_role('ROLE_SUBSCRIBED_USER')")
 */
class ImageController extends Controller
{
    /**
     * @Route("/image-upload/", name="image_upload")
     * @Method("POST")
     */
    public function uploadImage(Request $request)
    {
        if ($request->files->get('file') !== null) {
            $em = $this->getDoctrine()->getManager();

            $image = new Image();

            $image->setImageFile($request->files->get('file'));
            $image->setUpdatedAt(new \DateTime());

            $em->persist($image);
            $em->flush();

            return new JsonResponse($image->getId());
        }

        return new HttpException(500);
    }

    /**
     * @Route("/image-delete/{id}", name="image_delete")
     */
    public function deleteImage(Image $image)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();

        return new JsonResponse();
    }
}