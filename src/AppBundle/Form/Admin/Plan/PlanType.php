<?php

namespace AppBundle\Form\Admin\Plan;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PlanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('amount', MoneyType::class, array(
                'divisor' => 100,
                'label' => 'Amount in GBP',
                'currency' => 'gbp',
                'attr' => array('class' => 'form-control')
            ))
            ->add('planInterval', ChoiceType::class, array(
                'choices' => array(
                    'Day' => 'day',
                    'Week' => 'week',
                    'Month' => 'month',
                    'Year' => 'year'
                ),
                'attr' => array('class' => 'form-control')
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-block btn-primary')
            ))
        ;
    }
}