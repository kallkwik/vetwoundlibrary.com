<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Plan;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/admin/plan")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PlanController extends Controller
{
    /**
     * @Route("/", name="admin_plan_index",  defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_plan_index_paginated",
     *     requirements={"page"  : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Plan')->findAll();

        $paginator = $this->get('knp_paginator');
        $plans = $paginator->paginate($query, $page, Plan::NUM_ITEMS);
        $plans->setUsedRoute('admin_plan_index_paginated');

        return $this->render('admin/plan/index.html.twig', array(
            'plans' => $plans
        ));
    }

    /**
     * @Route("/show/{id}", name="admin_plan_show")
     * @Route("/new", name="admin_plan_new")
     */
    public function newShowAction(Plan $plan = null, Request $request)
    {
        if (isset($plan) === false) {
            $plan = new Plan();
        } else {
            $deleteForm = $this->createDeleteForm($plan);
            $renderParams['delete_form'] = $deleteForm->createView();
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\Plan\PlanType',
            $plan
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($plan);
            $em->flush();

            $this->addFlash('success', 'Plan has been saved.');

            $this->savePlanToStripe($plan);

            return $this->redirectToRoute('admin_plan_show', array(
                'id' => $plan->getId()
            ));
        }
        $renderParams['form'] = $form->createView();
        $renderParams['plan'] = $plan;

        return $this->render('admin/plan/show.html.twig', $renderParams);
    }

    public function prepareStripe()
    {
        $apiKey = $this->getParameter('stripe_private');
        $stripe = new \Stripe\Stripe;
        $stripe->setApiKey($apiKey);
    }

    public function savePlanToStripe(Plan $plan)
    {
        $this->prepareStripe();

        $stripePlan = new \Stripe\Plan;

        try {
            $stripePlan->retrieve($plan->getId());
        } catch(\Stripe\Error\InvalidRequest $e) {
            try {
                $stripePlan->create(array(
                    'amount' => $plan->getAmount(),
                    'interval' => $plan->getPlanInterval(),
                    'name' => $plan->getName(),
                    'currency' => $plan->getCurrency(),
                    'id' => $plan->getId()
                ));
            } catch(\Stripe\Error\InvalidRequest $e) {
                $this->addFlash('error', $e);
            }
        }
    }

    public function deletePlanFromStripe($id)
    {
        $this->prepareStripe();

        try {
            $stripePlan = \Stripe\Plan::retrieve($id);
            $stripePlan->delete();
        } catch(\Stripe\Error\InvalidRequest $e) {
            $this->addFlash('error', $e);
        }
    }

    /**
     * @Route("/{id}", name="admin_plan_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Plan $plan, Request $request)
    {
        $form = $this->createDeleteForm($plan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($plan);
            $em->flush();

            $this->deletePlanFromStripe($plan->getId());

            $this->addFlash('success', 'Plan deleted.');
        }

        return $this->redirectToRoute('admin_plan_index');
    }

    public function createDeleteForm(Plan $plan)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_plan_delete',
                array('id' => $plan->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Delete Plan',
                    'attr' => array(
                        'class' => 'btn btn-outline'
                    )
                )
            )
            ->getForm()
        ;
    }
}