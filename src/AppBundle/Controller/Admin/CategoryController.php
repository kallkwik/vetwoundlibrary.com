<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/product-category")
 * @Security("has_role('ROLE_ADMIN')")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/", name="admin_product_category_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_product_category_index_paginated",
     *     requirements={"page": "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Category')->findAll();
        $paginator = $this->get('knp_paginator');

        $category = $paginator->paginate($query, $page, Category::NUM_ITEMS);
        $category->setUsedRoute('admin_case_study_index_paginated');

        return $this->render('admin/category/index.html.twig',
            array(
                'categories' => $category,
                'sub_heading' => 'View all product categories.'
            )
        );
    }

    /**
     * @Route("/show/{slug}", name="admin_product_category_show")
     * @Route("/new", name="admin_product_category_new")
     */
    public function newShowAction(Category $category = null, Request $request)
    {
        if (isset($category) === false) {
            $category = new Category();
        } else {
            $deleteForm = $this->createDeleteForm($category);
            $renderParams['delete_form'] = $deleteForm->createView();
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\Category\CategoryType',
            $category
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Set slug to generated from title if no slug was entered
            $slug = $category->getSlug() === null ? $category->getName()
                : $category->getSlug();
            $category->setSlug(
                $this->get('slugger')->slugify($slug)
            );

            $em->persist($category);
            $em->flush();

            $this->addFlash('success', 'Category has been saved.');

            return $this->redirectToRoute('admin_product_category_show',
                array(
                    'slug' => $category->getSlug()
                )
            );
        }

        $renderParams['form'] = $form->createView();
        $renderParams['category'] = $category;

        return $this->render('admin/category/show.html.twig', $renderParams);
    }

    /**
     * @Route("/delete/{id}", name="admin_product_category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Category $category, Request $request)
    {
        if (!$category->getChildren()->isEmpty() || !$category->getProducts()->isEmpty()) {
            $this->addFlash(
                'error',
                'Category has children Categories/Products and cannot be deleted'
            );

            return $this->redirectToRoute('admin_product_category_show', array(
                'slug' => $category->getSlug(),
            ));
        }

        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($category);
            $em->flush();

            $this->addFlash('success', 'Category Successfully Deleted.');
        }

        return $this->redirectToRoute('admin_product_category_index');
    }

    public function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_product_category_delete',
                array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Delete Category',
                    'attr' => array(
                        'class' => 'btn btn-outline'
                    )
                )
            )
            ->getForm()
        ;
    }
}