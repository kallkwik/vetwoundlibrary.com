<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstLine', TextType::class)
            ->add('secondLine', TextType::class, array('required' => false))
            ->add('town', TextType::class, array('label' => false))
            ->add('county', TextType::class, array('required' => false))
            ->add('postcode', TextType::class)
        ;
    }
}