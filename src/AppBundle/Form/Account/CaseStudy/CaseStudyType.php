<?php

namespace AppBundle\Form\Account\CaseStudy;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaseStudyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('injuryName', TextType::class)
            ->add('petBreed', TextType::class)
            ->add('category', ChoiceType::class, array(
                'choices' => array(
                    'Please select Animal Type' => null,
                    'Small' => 1,
                    'Large' => 2,
                    'Exotic' => 3,
                ),
                'choices_as_values' => true,
            ))
            ->add('petBirthDate', DateType::class, array(
                'widget' => 'single_text'
            ))
            ->add('petSex', ChoiceType::class, array(
                'choices' => array(
                    'Please select pet gender' => null,
                    'Male' => true,
                    'Female' => false,
                ),
            ))
            ->add('tried', TextareaType::class, array(
                'required' => false
            ))
            ->add('woundLocation', TextType::class)
            ->add('woundDate', DateType::class, array(
                'widget' => 'single_text'
            ))
        ;
    }
}