<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vwl_contact")
 */
class Contact
{
    const NUM_ITEMS = 10;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     **/
    private $id;

    /**
     * @ORM\Column(type="string")
     **/
    private $email;

    /**
     * @ORM\Column(type="string")
     **/
    private $name;

    /**
     * @ORM\Column(type="string", name="phone_number", nullable=true)
     **/
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", nullable=true)
     **/
    private $subject;

    /**
     * @ORM\Column(type="text", nullable=true)
     **/
    private $content;

    /**
     * @ORM\Column(type="datetime", name="date_sent")
     **/
    private $dateSent;

    /**
     * @ORM\Column(type="string", name="ip_address")
     **/
    private $ipAddress;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="emails")
     */
    private $assigned;

    /**
     * @ORM\Column(type="boolean")
     */
    private $replied = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Contact
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Contact
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Contact
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set dateSent
     *
     * @param \DateTime $dateSent
     *
     * @return Contact
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;

        return $this;
    }

    /**
     * Get dateSent
     *
     * @return \DateTime
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     *
     * @return Contact
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set assigned
     *
     * @param \AppBundle\Entity\User $assigned
     *
     * @return Contact
     */
    public function setAssigned(\AppBundle\Entity\User $assigned = null)
    {
        $this->assigned = $assigned;

        return $this;
    }

    /**
     * Get assigned
     *
     * @return \AppBundle\Entity\User
     */
    public function getAssigned()
    {
        return $this->assigned;
    }

    /**
     * Set replied
     *
     * @param boolean $replied
     *
     * @return Contact
     */
    public function setReplied($replied)
    {
        $this->replied = $replied;

        return $this;
    }

    /**
     * Get replied
     *
     * @return boolean
     */
    public function getReplied()
    {
        return $this->replied;
    }
}
