<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{
    public function findOrdersAfterDate($date)
    {
        $date = $date->format('Y-m-d');

        $qb = $this->_em->createQueryBuilder();

        $query = $qb
            ->select('o')
            ->from($this->_entityName, 'o')
            ->where(
                $qb->expr()->gt('o.date', $date)
            )
            ->orderBy('o.date', 'DESC')
            ->setMaxResults(5)
            ->getQuery();

        return $query->getResult();
    }
}