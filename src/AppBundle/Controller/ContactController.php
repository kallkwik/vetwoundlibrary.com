<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/contact")
 */
class ContactController extends Controller
{
    /**
     * @Route("/", name="contact_new")
     */
    public function newAction(Request $request)
    {
        $contact = new Contact();
        $contact->setIpAddress($request->getClientIp());

        $form = $this->createForm(
            'AppBundle\Form\ContactType',
            $contact
        )->add('Send', 'Symfony\Component\Form\Extension\Core\Type\SubmitType');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $contact->setDateSent(new \DateTime("now"));

            $em->persist($contact);
            $em->flush();


            $this->get('email')->sendEmailToAdminForContact($contact);


            return $this->redirectToRoute('contact_success');
        }

        return $this->render('contact/new.html.twig',
            array(
                'contact' => $contact,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/thanks", name="contact_success")
     */
    public function successAction()
    {
        return $this->render('contact/success.html.twig');
    }
}