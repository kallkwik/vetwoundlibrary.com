<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;

class CaseStudyRepository extends EntityRepository
{
    public function findByAssignedHelpRequested()
    {
        $qb = $this->_em->createQuery('
            SELECT c
            FROM AppBundle:CaseStudy c
            WHERE c.assignedUser IS NOT NULL
            ORDER BY c.latestUpdate DESC
        ');

        return $this->filterByHelpNeeded($qb->getResult());
    }

    public function findByUnassignedHelpRequested()
    {
        $qb = $this->_em->createQuery('
            SELECT c
            FROM AppBundle:CaseStudy c
            WHERE c.assignedUser IS NULL
            ORDER BY c.latestUpdate DESC
        ');


        return $this->filterByHelpNeeded($qb->getResult());
    }

    public function filterByHelpNeeded($caseStudies)
    {
        $result = array();

        foreach ($caseStudies as $caseStudy) {
            if ($caseStudy->getLatestUpdate() !== null
                && $caseStudy->getLatestUpdate()->getHelpRequested() === true
            ) {
                array_push($result, $caseStudy);
            }
        }

        return $result;
    }
}