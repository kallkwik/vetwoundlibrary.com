<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\Length;

class SubscriptionType extends AbstractType
{
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', EntityType::class, array(
                'empty_data' => null,
                'expanded' => 'true',
                'attr' => array('class' => 'saved-address'),
                'class' => 'AppBundle:Address',
                'choice_label' => function ($address) {
                    return $address->getFirstLine() . ', ' . $address->getPostCode();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->orderBy('a.id', 'ASC')
                        ->where('IDENTITY(a.user) LIKE :user')
                        ->setParameter(
                            'user',
                            $this->tokenStorage->getToken()->getUser()->getId()
                        );
                },
                'required' => false,
                'placeholder' => 'New Address'
            ))
            ->add('firstLine', TextType::class, array(
                'required' => false,
                'label' => 'First Line of Address'
            ))
            ->add('secondLine', TextType::class, array(
                'required' => false,
                'label' => 'Second Line of Address'
            ))
            ->add('town', TextType::class, array(
                'required' => false,
            ))
            ->add('county', TextType::class, array(
                'required' => false
            ))
            ->add('postcode', TextType::class, array(
                'required' => false
            ))
            ->add('cardHolderName', TextType::class, array(
                'required' => false,
                'label' => 'Card Holder Name'
            ))
            ->add('cardNumber', NumberType::class, array(
                'label' => 'Card Number'
            ))
            ->add('cardExpDate', DateType::class, array(
                'widget' => 'choice',
                'label' => 'Expiry date (MM/YY)',
                'placeholder' => array(
                    'year' => 'Year', 'month' => 'Month'
                ),
                'years' => range(date('Y'), date('Y') + 10)

            ))
            ->add('cvc', NumberType::class, array(
                'label' => 'CVC',
                'constraints' => new Length(array(
                    'min' => 3,
                    'max' => 4
                ))
            ))
            ->add('submit', SubmitType::class)
        ;
    }
}