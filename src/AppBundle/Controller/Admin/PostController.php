<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Image;
use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/post")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="admin_post_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_post_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Post')->findBy(array(),
                array('dateSubmitted' => 'DESC')
            );

        $paginator = $this->get('knp_paginator');
        $posts = $paginator->paginate($query, $page, Post::NUM_ITEMS);
        $posts->setUsedRoute('admin_post_index_paginated');

        return $this->render('admin/post/index.html.twig',
            array(
                'posts' => $posts
            )
        );
    }

    /**
     * @Route("/show/{slug}", name="admin_post_show")
     * @Route("/new", name="admin_post_new")
     * @Method({"GET", "POST"})
     */
    public function newShowAction(Post $post = null, Request $request)
    {
        if (isset($post) === false) {
            $post = new Post();
        } else {
            $deleteForm = $this->createDeleteForm($post);
            $renderParams['delete_form'] = $deleteForm->createView();
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\Post\PostType',
            $post
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Set slug to generated from title if no slug was entered
            $slug = $post->getSlug() === null ? $post->getTitle()
                : $post->getSlug();
            $post->setSlug(
                $this->get('slugger')->slugify($slug)
            );

            // Set SEO Title to title if no SEO title was entered.
            if ($post->getSeoTitle() === null) {
                $post->setSeoTitle($post->getTitle());
            }

            $post->setUser($this->getUser());

            $em->persist($post);
            $em->flush();

            $this->addFlash('success', 'Post has been saved.');

            return $this->redirectToRoute('admin_post_show',
                array(
                    'slug' => $post->getSlug()
                )
            );
        }
        $renderParams['form'] = $form->createView();
        $renderParams['post'] = $post;

        return $this->render('admin/post/show.html.twig', $renderParams);
    }

    /**
     * @Route("/{id}", name="admin_post_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Post $post, Request $request)
    {
        $form = $this->createDeleteForm($post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->remove($post);
            $em->flush();

            $this->addFlash('success', 'Post Successfully Deleted.');
        }

        return $this->redirectToRoute('admin_post_index');
    }

    public function createDeleteForm(Post $post)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_post_delete',
                array('id' => $post->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Delete Post',
                    'attr' => array(
                        'class' => 'btn btn-outline'
                    )
                )
            )
            ->getForm()
        ;
    }

    /**
     * @Route("/remove-image/{id}", name="admin_delete_post_image")
     */
    public function deleteProductImage(Post $post)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($post->getImage());
        $post->setImage(null);
        $em->flush();

        return new Response();
    }
}