<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ResponseSubscriptionListener
{
    private $tokenStorage;
    private $em;
    private $user;
    private $authorizationChecker;

    public function __construct(
        TokenStorage $tokenStorage,
        EntityManager $entityManager,
        AuthorizationChecker $authorizationChecker
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->em = $entityManager;
        $this->authorizationChecker = $authorizationChecker;

        $this->getUser();
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($this->tokenStorage->getToken() !== null) {
            if($this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')
                || $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')
            ) {
                $this->updateUserSubscriptionStatus();
            }
        }
    }

    public function getUser()
    {
        if ($this->tokenStorage->getToken() !== null) {
            $this->user = $this->tokenStorage->getToken()->getUser();
        }
    }

    private function isUserStillSubscribed()
    {
        $activeSub = $this->user->getActiveSubscription();

        if ($activeSub === null) {
            return false;
        }

        $dateTime = new \DateTime();

        if ($activeSub->getStatus() == "canceled"
            && $activeSub->getPeriodEnd() <= $dateTime) {
            return false;
        }

        return true;
    }

    /**
     * This will take a check from isUserStillSubscribed method and
     * update the user subscription status/role to unsubscribed if
     * their subscription has lapsed.
     *
     * It's called from Security Listener.
     *
     * This method can stay just in case the webhook fails to update the user subscription status.
     */
    public function updateUserSubscriptionStatus()
    {
        if ($this->isUserStillSubscribed() === false) {
            if ($this->user->getActiveSubscription() !== null) {
                $this->user->setActiveSubscription(null);
            }

            $this->user->removeRole('ROLE_SUBSCRIBED_USER');
            $this->em->persist($this->user);
            $this->em->flush();
        }
    }
}