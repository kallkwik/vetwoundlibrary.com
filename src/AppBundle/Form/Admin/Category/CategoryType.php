<?php

namespace AppBundle\Form\Admin\Category;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('slug', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'required' => false
            ))
            ->add('parent', EntityType::class, array(
                'class' => 'AppBundle:Category',
                'attr' => array('class' => 'form-control'),
                'choice_label' => function ($category) {
                    return $category->getName();
                },
                'required' => false,
                'placeholder' => 'None selected'
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-block btn-primary'),
                'label' => 'Save Category'
            ))
        ;
    }
}
