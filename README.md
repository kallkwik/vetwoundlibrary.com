# Vet Wound Library - built using Symfony 3.0
___

### To install a new copy
1. Open terminal.
2. Navigate to root folder of Vet Wound Library.
3. Download and install dependencies.
   ``` shell
   composer update
   ```
4. Add Stripe keys and ensure database credentials are correct in app/config/parameters.yml
5. Generate database schema.
   ``` shell
   php bin/console doctrine:schema:update --force
   ```
6. Generate Fixtures.
   ``` shell
   php bin/console doctrine:fixtures:load --append
   ```

### To start this server locally for development
1. Open terminal.
2. Navigate to root folder of Vet Wound Library.
2. Start Symfony web server.
    ``` shell
    php bin/console server:run
    ```

### Update Database schema - need to do when pulling changes
1. Open Terminal.
2. Navigate to root folder of Vet Wound Library.
3. Update database schema.
   ``` shell
   php bin/console doctrine:schema:update --force
   ```

### Generate Fixtures - default values in database
1. Open Terminal.
2. Navigate to root folder of Vet Wound Library.
3. Generate Fixtures.
   ``` shell
   php bin/console doctrine:fixtures:load --append
   ```

### Clear cache - need to do when making template changes
1. Open Terminal.
2. Navigate to root folder of Vet Wound Library.

## Notes:
The root website folder refers to your local copy or copy on production server (access through SSH).

ssh kallkwik@178.62.34.192