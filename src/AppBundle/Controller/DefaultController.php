<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CaseStudy;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $doctrine = $this->getDoctrine();

        $caseStudies = $doctrine->getRepository('AppBundle:CaseStudy')
            ->findBy(array(
                    'published' => CaseStudy::PUBLIC_VISIBLE
            ))
        ;
        shuffle($caseStudies);

        $staticPage = $doctrine->getRepository('AppBundle:StaticPage')
            ->findOneBy(array(
                'route' => 'homepage'
            ))
        ;

        return $this->render('default/index.html.twig', array(
            'case_studies' => array_slice($caseStudies, 0, 3),
            'homepage' => $staticPage,
        ));
    }
}
