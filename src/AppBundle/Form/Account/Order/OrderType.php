<?php

namespace AppBundle\Form\Account\Order;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $product = $options['product'];

        $builder
            ->add('wholesalerName', TextType::class, array(
                'required' => false
            ))
            ->add('wholesalerAccountNumber', TextType::class, array(
                'required' => false
            ))
            ->add('distributor', EntityType::class, array(
                'class' => 'AppBundle:Distributor',
                'attr' => array('class' => 'form-control'),
                'label' => 'Select preferred Distributor',
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) use ($product) {
                    return $er->createQueryBuilder('d')
                        ->innerJoin('d.products', 'p')
                        ->where('p.id = :product_id')
                        ->setParameter('product_id', $product->getId())
                        ->orderBy('d.sponsored', 'DESC');
                }
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-block btn-primary'),
                'label' => 'Place Order',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'product' => null
        ));
    }
}