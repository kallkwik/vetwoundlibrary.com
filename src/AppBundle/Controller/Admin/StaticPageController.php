<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\StaticPage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/static-page")
 */
class StaticPageController extends Controller
{
    /**
     * @Route("/", name="admin_static_page_index")
     */
    public function indexAction()
    {
        $staticPages = $this->getDoctrine()->getRepository('AppBundle:StaticPage')->findAll();

        return $this->render('admin/static_page/index.html.twig', array(
            'static_pages' => $staticPages
        ));
    }

    /**
     * @Route("/{slug}", name="admin_static_page_show")
     */
    public function showEditAction(StaticPage $staticPage, Request $request)
    {
        $form = $this->createForm(
            'AppBundle\Form\Admin\StaticPage\StaticPageType',
            $staticPage
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();

            $images = $request->request->get('images');

            if ($images !== null) {
                $imageRepository = $this->getDoctrine()->getRepository('AppBundle:Image');

                foreach ($images as $id) {
                    $image = $imageRepository->find($id);
                    $image->setStaticPage($staticPage);
                    $em->persist($image);
                }
            }

            $em->persist($staticPage);
            $em->flush();

            $this->addFlash('success', 'Static Page has been updated.');
        }

        return $this->render('admin/static_page/show.html.twig', array(
            'static_page' => $staticPage,
            'request' => $request,
            'form' => $form->createView(),
        ));
    }
}