<?php

namespace AppBundle\Form\Admin\CaseStudy;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class SummaryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('summary', TextareaType::class, array(
                    'attr' => array('class' => 'form-control')
                )
            )
            ->add('submit', SubmitType::class, array(
                    'label' => 'Save Summary',
                    'attr' => array(
                        'class' => 'btn btn-block btn-primary'
                    )
                )
            )
        ;
    }
}