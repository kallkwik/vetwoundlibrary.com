<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vwl_case_study_update")
 */
class CaseStudyUpdate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", name="date_updated")
     */
    private $date;

    /**
     * @ORM\Column(type="string", name="update_content")
     */
    private $content;

    /**
     * @ORM\Column(type="string", name="wound_size", nullable=true)
     */
    private $woundSize;

    /**
     * @ORM\Column(type="string", name="tissue_status", nullable=true)
     */
    private $tissueStatus;

    /**
     * @ORM\Column(type="boolean", name="is_deleted")
     */
    private $isDeleted = false;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="caseStudyUpdate")
     */
    private $images;

    /**
     * @ORM\OneToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="featured_image_id", referencedColumnName="id")
     */
    private $featuredImage;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="CaseStudy", inversedBy="caseStudyUpdate")
     * @ORM\JoinColumn(name="case_study_id", referencedColumnName="id")
     */
    private $caseStudy;

    /**
     * @ORM\Column(type="boolean")
     */
    private $helpRequested = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return CaseStudyUpdate
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CaseStudyUpdate
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set woundSize
     *
     * @param string $woundSize
     *
     * @return CaseStudyUpdate
     */
    public function setWoundSize($woundSize)
    {
        $this->woundSize = $woundSize;

        return $this;
    }

    /**
     * Get woundSize
     *
     * @return string
     */
    public function getWoundSize()
    {
        return $this->woundSize;
    }

    /**
     * Set tissueStatus
     *
     * @param string $tissueStatus
     *
     * @return CaseStudyUpdate
     */
    public function setTissueStatus($tissueStatus)
    {
        $this->tissueStatus = $tissueStatus;

        return $this;
    }

    /**
     * Get tissueStatus
     *
     * @return string
     */
    public function getTissueStatus()
    {
        return $this->tissueStatus;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return CaseStudyUpdate
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Image $image
     */
    public function removeImage(\AppBundle\Entity\Image $image)
    {
        $this->images->remove($image);
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return Product
     */
    public function addImage(\AppBundle\Entity\Image $image)
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }

        return $this;
    }

    /**
     * Set Images
     * @param $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set featuredImage
     *
     * @param \AppBundle\Entity\Image $featuredImage
     *
     * @return CaseStudyUpdate
     */
    public function setFeaturedImage(\AppBundle\Entity\Image $featuredImage = null)
    {
        $this->featuredImage = $featuredImage;

        return $this;
    }

    /**
     * Get featuredImage
     *
     * @return \AppBundle\Entity\Image
     */
    public function getFeaturedImage()
    {
        return $this->featuredImage;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return CaseStudyUpdate
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set caseStudy
     *
     * @param \AppBundle\Entity\CaseStudy $caseStudy
     *
     * @return CaseStudyUpdate
     */
    public function setCaseStudy(\AppBundle\Entity\CaseStudy $caseStudy = null)
    {
        $this->caseStudy = $caseStudy;

        return $this;
    }

    /**
     * Get caseStudy
     *
     * @return \AppBundle\Entity\CaseStudy
     */
    public function getCaseStudy()
    {
        return $this->caseStudy;
    }

    /**
     * Set helpRequested
     *
     * @param boolean $helpRequested
     *
     * @return CaseStudyUpdate
     */
    public function setHelpRequested($helpRequested)
    {
        $this->helpRequested = $helpRequested;

        return $this;
    }

    /**
     * Get helpRequested
     *
     * @return boolean
     */
    public function getHelpRequested()
    {
        return $this->helpRequested;
    }
}
