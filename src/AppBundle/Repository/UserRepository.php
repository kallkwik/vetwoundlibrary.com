<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * Find users by role and if they have been set as published.
     *
     * Will get all users if only role is given.
     */
    public function findByRole($role, $isSetPublished = null)
    {
        $qb = $this->_em->createQueryBuilder();

        if ($isSetPublished !== null) {
            $qb->select('u')
                ->from($this->_entityName, 'u')
                ->where('u.roles LIKE :roles')
                ->andWhere('u.published LIKE :published ')
                ->setParameter('roles', '%"' . $role . '"%')
                ->setParameter('published', $isSetPublished);
        } else {
            $qb->select('u')
                ->from($this->_entityName, 'u')
                ->where('u.roles LIKE :roles')
                ->setParameter('roles', '%"' . $role . '"%');
        }

        return $qb->getQuery()->getResult();
    }

    public function findAllUsers()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('u.roles NOT LIKE :role_angel')
            ->andWhere('u.roles NOT LIKE :role_expert')
            ->andWhere('u.roles NOT LIKE :role_admin')
            ->setParameters(array(
                'role_angel' => '%"ROLE_ANGEL"%',
                'role_expert' => '%"ROLE_EXPERT"%',
                'role_admin' => '%"ROLE_ADMIN"%',
            ))
            ->addOrderBy('u.activeSubscription', 'DESC')
            ->addOrderBy('u.username', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findAllAdminEmails()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('u.email')
            ->from($this->_entityName, 'u')
            ->where('u.roles LIKE :role_admin')
            ->setParameter('role_admin', '%"ROLE_ADMIN"%')
            ->groupBy('u.email')
        ;

        $users = $qb->getQuery()->getResult();
        $emails = array();

        foreach($users as $user) {
            $emails[] = $user['email'];
        }

        return $emails;
    }
}