<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/account")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends Controller
{
    /**
     * @Route("/{role}", name="admin_user_index", defaults={"page" = 1})
     * @Route("/{role}/page/{page}", name="admin_user_index_paginated", requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($role = null, $page)
    {
        switch($role) {
            case 'expert':
                $query = $this->getDoctrine()
                    ->getRepository('AppBundle:User')
                    ->findByRole('ROLE_EXPERT');
                $renderParams['heading'] = 'View Experts';
                $renderParams['show_subscription'] = false;
                break;
            case 'angel':
                $query = $this->getDoctrine()
                    ->getRepository('AppBundle:User')
                    ->findByRole('ROLE_ANGEL');
                $renderParams['heading'] = 'View Angels';
                $renderParams['show_subscription'] = false;
                break;
            case 'user':
                $query = $this->getDoctrine()
                    ->getRepository('AppBundle:User')
                    ->findAllUsers();
                $renderParams['heading'] = 'View Users';
                $renderParams['show_subscription'] = true;
                break;
        }

        $paginator = $this->get('knp_paginator');
        $users = $paginator->paginate($query, $page, User::NUM_ITEMS);
        $users->setUsedRoute('admin_user_index_paginated');

        $renderParams['users'] = $users;

        return $this->render('admin/user/index.html.twig', $renderParams);
    }


    /**
     * @Route("/show/{id}", name="admin_user_show")
     */
    public function showAction(User $user, Request $request)
    {
        $form = $this->createForm('AppBundle\Form\Admin\User\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'User has been saved.');

            return $this->redirectToRoute('admin_user_show',
                array(
                    'id' => $user->getId()
                )
            );
        }

        $addresses = $this->getDoctrine()->getRepository('AppBundle:Address')
            ->findBy(array('user' => $user));

        return $this->render('admin/user/show.html.twig', array(
            'addresses' => $addresses,
            'user' => $user,
            'form' => $form->createView(),
        ));
    }
}