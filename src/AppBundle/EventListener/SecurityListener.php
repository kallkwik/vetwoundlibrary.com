<?php

namespace AppBundle\EventListener;

use AppBundle\Controller\Account\SubscriptionController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class SecurityListener
{
    private $router;
    private $authorizationChecker;

    public function __construct(
        Router $router,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof AccessDeniedHttpException) {
            $this->handleAccessDenied($event);
        }
    }

    public function handleAccessDenied(GetResponseForExceptionEvent $event)
    {
        $route = null;

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            $this->authorizationChecker->isGranted('ROLE_EXPERT') ||
            $this->authorizationChecker->isGranted('ROLE_ANGEL')
        ) {
            $route = 'admin_index';
        } else if ($this->authorizationChecker->isGranted('ROLE_SUBSCRIBED_USER') === false &&
            $this->authorizationChecker->isGranted('ROLE_USER')
        ) {
            $route = 'plan_index';
        };
        $response = new RedirectResponse(
            $this->router->generate($route)
        );
        $event->setResponse($response);
    }
}