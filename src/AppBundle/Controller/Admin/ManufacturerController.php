<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Manufacturer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/manufacturer")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ManufacturerController extends Controller
{
    /**
     * @Route("/", name="admin_manufacturer_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_manufacturer_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Manufacturer')->findAll();

        $paginator = $this->get('knp_paginator');
        $manufacturers = $paginator->paginate($query, $page,
            Manufacturer::NUM_ITEMS);
        $manufacturers->setUsedRoute('admin_manufacturer_index_paginted');

        return $this->render('admin/manufacturer/index.html.twig',
            array(
                'manufacturers' => $manufacturers
            )
        );
    }

    /**
     * @Route("/show/{id}", name="admin_manufacturer_show")
     * @Route("/new", name="admin_manufacturer_new")
     */
    public function newShowAction(
        Manufacturer $manufacturer = null,
        Request $request
    ) {
        if (isset($manufacturer) === false) {
            $manufacturer = new Manufacturer();
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\Manufacturer\ManufacturerType',
            $manufacturer
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($manufacturer);
            $em->flush();

            $this->addFlash('success', 'Manufacturer has been saved.');

            return $this->redirectToRoute('admin_manufacturer_show',
                array(
                    'id' => $manufacturer->getId()
                )
            );
        }

        return $this->render('admin/manufacturer/show.html.twig',
            array(
                'manufacturer' => $manufacturer,
                'form' => $form->createView(),
                'delete_form' => $this->createDeleteForm($manufacturer)->createView(),
            )
        );
    }

    /**
     * @Route("/delete/{id}", name="admin_manufacturer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Manufacturer $manufacturer, Request $request)
    {
        if (!$manufacturer->getProducts()->isEmpty()) {
            $this->addFlash('error', 'Manufacturer has products and cannot be deleted');

            return $this->redirectToRoute('admin_manufacturer_show', array(
                'id' => $manufacturer->getId(),
            ));
        }

        $form = $this->createDeleteForm($manufacturer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($manufacturer);
            $em->flush();

            $this->addFlash('success', 'Manufacturer Successfully Deleted.');
        }

        return $this->redirectToRoute('admin_manufacturer_index');
    }

    public function createDeleteForm(Manufacturer $manufacturer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_manufacturer_delete',
                array('id' => $manufacturer->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Delete Manufacturer',
                    'attr' => array(
                        'class' => 'btn btn-outline'
                    )
                )
            )
            ->getForm()
        ;
    }
}