<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\CaseStudy;
use AppBundle\Entity\CaseStudyUpdate;
use AppBundle\Entity\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/admin/case-study")
 * @Security("has_role('ROLE_EXPERT') or has_role('ROLE_ANGEL') or has_role('ROLE_ADMIN')")
 */
class CaseStudyController extends Controller
{
    /**
     * @Route("/", name="admin_case_study_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_case_study_index_paginated",
     *     requirements={"page": "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $authChecker = $this->get('security.authorization_checker');

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            $query = $this->getDoctrine()
                ->getRepository('AppBundle:CaseStudy')->findBy(
                    array('archived' => false),
                    array('latestUpdate' => 'DESC')
                );
        } else {
            $query = $this->getDoctrine()
                ->getRepository('AppBundle:CaseStudy')->findBy(
                    array(
                        'assignedUser' => $this->getUser(),
                        'archived' => false
                    )
                );
        }

        $paginator = $this->get('knp_paginator');

        $caseStudies = $paginator->paginate($query, $page, CaseStudy::NUM_ITEMS);
        $caseStudies->setUsedRoute('admin_case_study_index_paginated');

        return $this->render('admin/case_study/index.html.twig',
            array(
                'case_studies' => $caseStudies,
                'sub_heading' => 'View all active Case Studies'
            )
        );
    }

    /**
     * @Route("/inactive", name="admin_case_study_inactive_index",
     *     defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_case_study_inactive_index_paginated",
     *     requirements={"page": "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexInactiveAction($page)
    {
        $authChecker = $this->get('security.authorization_checker');

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            $query = $this->getDoctrine()
                ->getRepository('AppBundle:CaseStudy')->findBy(
                    array('archived' => true),
                    array('latestUpdate' => 'DESC')
                );
        } else {
            $query = $this->getDoctrine()
                ->getRepository('AppBundle:CaseStudy')->findBy(
                    array(
                        'assignedUser' => $this->getUser(),
                        'archived' => true
                    )
                );
        }

        $paginator = $this->get('knp_paginator');

        $caseStudies = $paginator->paginate($query, $page, CaseStudy::NUM_ITEMS);
        $caseStudies->setUsedRoute('admin_case_study_index_paginated');

        return $this->render('admin/case_study/index.html.twig',
            array(
                'case_studies' => $caseStudies,
                'sub_heading' => 'View all inactive Case Studies'
            )
        );
    }

    /**
     * @Route("/help-requested", name="admin_case_study_help_index")
     */
    public function indexHelpAction()
    {
        $caseStudiesAssigned = $this->getDoctrine()
            ->getRepository('AppBundle:CaseStudy')
            ->findByAssignedHelpRequested();

        $caseStudiesUnassigned = $this->getDoctrine()
            ->getRepository('AppBundle:CaseStudy')
            ->findByUnassignedHelpRequested();

        return $this->render('admin/case_study/index_help.html.twig', array(
            'case_studies_assigned' => $caseStudiesAssigned,
            'case_studies_unassigned' => $caseStudiesUnassigned
        ));
    }

    /**
     * @Route("/view/{slug}", name="admin_case_study_show")
     *
     */
    public function showAction(CaseStudy $caseStudy, Request $request)
    {
        $form = $this->createForm('AppBundle\Form\Admin\CaseStudy\CaseStudyType',
            $caseStudy
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($caseStudy);
            $em->flush();

            $this->addFlash('success', 'Case study successfully updated.');
        }

        return $this->render('admin/case_study/show.html.twig',
            array(
                'case_study' => $caseStudy,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/edit/{slug}", name="admin_case_study_edit")
     * @Route("/new", name="admin_case_study_new")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newEditAction(CaseStudy $caseStudy = null, Request $request)
    {
        if (isset($caseStudy) === false) {
            $caseStudy = new CaseStudy();
            $caseStudy->setUser($this->getUser());
            $title = 'New Case Study';
        } else {
            $title = 'Edit Case Study';
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\CaseStudy\CaseStudyNewEditType',
            $caseStudy
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getdoctrine()->getManager();

            $caseStudy->setSlug(
                $this->get('slugger')->slugify($caseStudy->getInjuryName())
            );

            if ($em->getRepository('AppBundle:CaseStudyUpdate')->findOneBy(
                array('caseStudy' => $caseStudy))
            ) {
                $em->persist($caseStudy);
                $em->flush();

                $this->addFlash('success', 'Case Study saved.');

                return $this->redirectToRoute('admin_case_study_show', array(
                    'slug' => $caseStudy->getSlug()
                ));
            } else {
                $caseStudy->setDateSubmitted(new \DateTime());

                $em->persist($caseStudy);
                $em->flush();

                return $this->redirectToRoute('admin_case_study_update_new',
                    array(
                        'slug' => $caseStudy->getSlug()
                    )
                );
            }
        }

        return $this->render('admin/case_study/new.html.twig',
            array(
                'case_study' => $caseStudy,
                'form' => $form->createView(),
                'title' => $title
            )
        );
    }

    /**
     * @Route("/{slug}/update/new", name="admin_case_study_update_new")
     */
    public function newUpdateAction(CaseStudy $caseStudy, Request $request)
    {
        $caseStudyUpdate = new CaseStudyUpdate();
        $caseStudyUpdate->setUser($this->getUser());
        $caseStudyUpdate->setDate(new \DateTime());
        $caseStudyUpdate->setCaseStudy($caseStudy);

        $form = $this->createForm(
            'AppBundle\Form\Admin\CaseStudy\UpdateType',
            $caseStudyUpdate
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $images = $request->request->get('images');

            if ($images !== null) {
                $imageRepository = $this->getDoctrine()->getRepository('AppBundle:Image');

                foreach ($images as $id) {
                    $image = $imageRepository->find($id);
                    $image->setCaseStudyUpdate($caseStudyUpdate);
                    $em->persist($image);
                }
            }

            $caseStudy->setLatestUpdate($caseStudyUpdate);

            $em->persist($caseStudy);
            $em->persist($caseStudyUpdate);
            $em->flush();

            $this->addFlash('success', 'Update saved.');

            return $this->redirectToRoute('admin_case_study_show',
                array(
                    'slug' => $caseStudy->getSlug()
                )
            );
        }

        return $this->render('admin/case_study/new_update.html.twig',
            array(
                'case_study' => $caseStudy,
                'case_study_update' => $caseStudyUpdate,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/{slug}/update/{caseStudyUpdateId}", name="admin_case_study_update_edit")
     */
    public function editUpdateAction(CaseStudy $caseStudy, $caseStudyUpdateId, Request $request)
    {
        $caseStudyUpdate = $this->getDoctrine()
            ->getRepository('AppBundle:CaseStudyUpdate')
            ->find($caseStudyUpdateId);

        if ($caseStudyUpdate->getCaseStudy() !== $caseStudy) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\CaseStudy\UpdateType',
            $caseStudyUpdate
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $images = $request->request->get('images');

            if ($images !== null) {
                $imageRepository = $this->getDoctrine()->getRepository('AppBundle:Image');

                foreach ($images as $id) {
                    $image = $imageRepository->find($id);
                    $image->setCaseStudyUpdate($caseStudyUpdate);
                    $em->persist($image);
                }
            }

            $em->persist($caseStudyUpdate);
            $em->flush();

            $this->addFlash('success', 'Update saved.');

            return $this->redirectToRoute('admin_case_study_show',
                array(
                    'slug' => $caseStudy->getSlug()
                )
            );
        }

        return $this->render('admin/case_study/new_update.html.twig',
            array(
                'case_study' => $caseStudy,
                'case_study_update' => $caseStudyUpdate,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/summary/{slug}", name="admin_case_study_summary_change")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function changeSummaryAction(CaseStudy $caseStudy, Request $request)
    {
        $form = $this->createForm(
            'AppBundle\Form\Admin\CaseStudy\SummaryType',
            $caseStudy
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($caseStudy);
            $em->flush();

            $this->addFlash('success', 'Summary successfully updated.');

            return $this->redirectToRoute('admin_case_study_show',
                array(
                    'slug' => $caseStudy->getSlug()
                )
            );
        }

        return $this->render('admin/case_study/summary.html.twig',
            array(
                'case_study' => $caseStudy,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/publish/{slug}", name="admin_case_study_publish")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function publishAction(CaseStudy $caseStudy)
    {
        $caseStudy->setPublished(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($caseStudy);
        $em->flush();

        $this->addFlash('success', 'Case Study has been Published');

        return $this->redirectToRoute('admin_case_study_show',
            array(
                'slug' => $caseStudy->getSlug()
            )
        );
    }

    /**
     * @Route("/unpublish/{slug}", name="admin_case_study_unpublish")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function unpublishAction(CaseStudy $caseStudy)
    {
        $caseStudy->setPublished(false);

        $em = $this->getDoctrine()->getManager();
        $em->persist($caseStudy);
        $em->flush();

        $this->addFlash('success', 'Case Study has been Unpublished');

        return $this->redirectToRoute('admin_case_study_show',
            array(
                'slug' => $caseStudy->getSlug()
            )
        );
    }

    /**
     * @Route("/archive/{slug}", name="admin_case_study_archive")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function archiveAction(CaseStudy $caseStudy)
    {
        $caseStudy->setArchived(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($caseStudy);
        $em->flush();

        $this->addFlash('success', 'Case Study has been Archived');

        return $this->redirectToRoute('admin_case_study_show',
            array(
                'slug' => $caseStudy->getSlug()
            )
        );
    }

    /**
     * @Route("/unarchive/{slug}", name="admin_case_study_unarchive")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function unarchiveAction(CaseStudy $caseStudy)
    {
        $caseStudy->setArchived(false);

        $em = $this->getDoctrine()->getManager();
        $em->persist($caseStudy);
        $em->flush();

        $this->addFlash('success', 'Case Study has been Unarchived');

        return $this->redirectToRoute('admin_case_study_show',
            array(
                'slug' => $caseStudy->getSlug()
            )
        );
    }
}