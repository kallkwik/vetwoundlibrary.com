<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="vwl_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email is already in use.")
 * @UniqueEntity(fields="username", message="Username is already in use.")
 */
class User extends BaseUser implements UserInterface, \Serializable
{
    const NUM_ITEMS = 10;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max = 4096)
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="string", name="first_name", nullable=true)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", name="last_name", nullable=true)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $stripeId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Address", mappedBy="user",
     *      cascade={"persist", "remove"})
     */
    private $addresses;

    /**
     * @ORM\OneToOne(targetEntity="Image", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published = false;

    /**
     * @ORM\OneToOne(targetEntity="Subscription")
     */
    private $activeSubscription;

    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="assigned")
     */
    private $emails;

    /**
     * @ORM\OneToMany(targetEntity="CaseStudy", mappedBy="assignedUser")
     */
    private $assignedCaseStudies;

    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="user")
     */
    private $orders;

    public function __construct()
    {
        parent::__construct();
        $this->addresses = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password,
        ) = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        if(empty($this->roles)) {
            $roles[] = 'ROLE_USER';

            return array_unique($roles);
        }

        return $this->roles;
    }

    public function setRole(array $roles)
    {
        $this->roles = $roles;
    }

    public function addRole($role)
    {
        if (is_array($role) === true
            || array_search($role, $this->roles)
        ) {
            return;
        }

        $this->roles = array_push($this->roles, $role);
    }

    public function removeRole($role)
    {
        $index = array_search($role, $this->roles);

        if ($index !== false) {
            unset($this->roles[$index]);
        }
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Add address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return User
     */
    public function addAddress(\AppBundle\Entity\Address $address)
    {
        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \AppBundle\Entity\Address $address
     */
    public function removeAddress(\AppBundle\Entity\Address $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Set stripeId
     *
     * @param string $stripeId
     *
     * @return User
     */
    public function setStripeId($stripeId)
    {
        $this->stripeId = $stripeId;

        return $this;
    }

    /**
     * Get stripeId
     *
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * Set image
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return User
     */
    public function setImage(\AppBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return User
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Add email
     *
     * @param \AppBundle\Entity\contact $email
     *
     * @return User
     */
    public function addEmail(\AppBundle\Entity\contact $email)
    {
        $this->emails[] = $email;

        return $this;
    }

    /**
     * Remove email
     *
     * @param \AppBundle\Entity\contact $email
     */
    public function removeEmail(\AppBundle\Entity\contact $email)
    {
        $this->emails->removeElement($email);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add assignedCaseStudy
     *
     * @param \AppBundle\Entity\CaseStudy $assignedCaseStudy
     *
     * @return User
     */
    public function addAssignedCaseStudy(\AppBundle\Entity\CaseStudy $assignedCaseStudy)
    {
        $this->assignedCaseStudies[] = $assignedCaseStudy;

        return $this;
    }

    /**
     * Remove assignedCaseStudy
     *
     * @param \AppBundle\Entity\CaseStudy $assignedCaseStudy
     */
    public function removeAssignedCaseStudy(\AppBundle\Entity\CaseStudy $assignedCaseStudy)
    {
        $this->assignedCaseStudies->removeElement($assignedCaseStudy);
    }

    /**
     * Get assignedCaseStudies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignedCaseStudies()
    {
        return $this->assignedCaseStudies;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set activeSubscription
     *
     * @param \AppBundle\Entity\Subscription $activeSubscription
     *
     * @return User
     */
    public function setActiveSubscription(\AppBundle\Entity\Subscription $activeSubscription = null)
    {
        $this->activeSubscription = $activeSubscription;

        return $this;
    }

    /**
     * Get activeSubscription
     *
     * @return \AppBundle\Entity\Subscription
     */
    public function getActiveSubscription()
    {
        return $this->activeSubscription;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\Order $order
     *
     * @return User
     */
    public function addOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\Order $order
     */
    public function removeOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }
}
