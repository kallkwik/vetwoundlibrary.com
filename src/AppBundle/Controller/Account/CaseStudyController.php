<?php

namespace AppBundle\Controller\Account;

use AppBundle\Entity\CaseStudy;
use AppBundle\Entity\CaseStudyUpdate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/account/case-study")
 * @Security("has_role('ROLE_SUBSCRIBED_USER')")
 */
class CaseStudyController extends Controller
{
    /**
     * @Route("/", name="case_study_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="case_study_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs
            ->addItem('Home', $this->get('router')->generate('homepage'));
        $breadcrumbs
            ->addItem('Account', $this->get('router')->generate('account_index'));
        $breadcrumbs->addItem('Case Study');

        $query = $this->getDoctrine()
            ->getRepository('AppBundle:CaseStudy')->findBy(
                array('user' => $this->getUser()),
                array('latestUpdate' => 'DESC')
            );

        $paginator = $this->get('knp_paginator');
        $caseStudies = $paginator->paginate($query, $page, CaseStudy::NUM_ITEMS);
        $caseStudies->setUsedRoute('case_study_index_paginated');

        return $this->render('account/case_study/index.html.twig',
            array('case_studies' => $caseStudies)
        );
    }

    /**
     * @Route("/view/{slug}", name="case_study_single")
     */
    public function showAction(CaseStudy $caseStudy)
    {
        if ($caseStudy->getUser() !== $this->getUser()) {
            throw $this->createNotFoundException();
        }

        return $this->render('account/case_study/show.html.twig',
            array('case_study' => $caseStudy)
        );
    }

    /**
     * @Route("/edit/{slug}", name="case_study_edit")
     * @Route("/new", name="case_study_new")
     * @Method({"GET", "POST"})
     */
    public function newEditAction(CaseStudy $caseStudy = null, Request $request)
    {
        if (isset($caseStudy) === false) {
            $caseStudy = new CaseStudy();
            $caseStudy->setUser($this->getUser());
            $title = 'New Case Study';
        } else {
            $title = 'Edit Case Study';

            if ($caseStudy->getUser() !== $this->getUser()) {
                throw $this->createNotFoundException();
            }
        }

        $form =
            $this->createForm(
                'AppBundle\Form\Account\CaseStudy\CaseStudyType', $caseStudy)
            ->add('createNewCaseStudy',
                'Symfony\Component\Form\Extension\Core\Type\SubmitType',
                array('label' => 'Save Case Study'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $caseStudy->setSlug(
                $this->get('slugger')->slugify($caseStudy->getInjuryName())
            );

            $em = $this->getdoctrine()->getManager();

            if ($em->getRepository('AppBundle:CaseStudyUpdate')->findOneBy(
                array('caseStudy' => $caseStudy))
            ) {
                $em->persist($caseStudy);
                $em->flush();

                $this->addFlash('success', 'Case Study saved.');

                return $this->redirectToRoute('case_study_single', array(
                    'slug' => $caseStudy->getSlug()
                ));
            } else {
                $caseStudy->setDateSubmitted(new \DateTime());

                $em->persist($caseStudy);
                $em->flush();

                return $this->redirectToRoute('case_study_update_new',
                    array(
                        'caseStudySlug' => $caseStudy->getSlug()
                    )
                );
            }
        }

        return $this->render('account/case_study/new.html.twig',
            array(
                'case_study' => $caseStudy,
                'form' => $form->createView(),
                'title' => $title
            )
        );
    }

    /**
     * @Route("/{caseStudySlug}/update/new", name="case_study_update_new")
     * @Route("/{caseStudySlug}/update/{caseStudyUpdateId}", name="case_study_update_edit")
     */
    public function newUpdateAction($caseStudySlug,
                                    $caseStudyUpdateId = null,
                                    Request $request
    )
    {
        $caseStudy = $this->getDoctrine()
            ->getRepository('AppBundle:CaseStudy')
            ->findOneBy(array('slug' => $caseStudySlug, 'user' => $this->getUser()));

        if ($caseStudy === null) {
            throw $this->createNotFoundException();
        }

        if (isset($caseStudyUpdateId) === false) {
            $caseStudyUpdate = new CaseStudyUpdate();
            $caseStudyUpdate->setUser($this->getUser());
            $caseStudyUpdate->setDate(new \DateTime());
            $caseStudyUpdate->setCaseStudy($caseStudy);

            $caseStudy->setLatestUpdate($caseStudyUpdate);
        } else {
            $caseStudyUpdate = $this->getDoctrine()
                ->getRepository('AppBundle:CaseStudyUpdate')
                ->findOneBy(array(
                    'id' => $caseStudyUpdateId,
                    'user' => $this->getUser()
                ));

            if ($caseStudyUpdate->getCaseStudy() !== $caseStudy) {
                throw $this->createNotFoundException();
            }
        }

        $form = $this->createForm(
            'AppBundle\Form\Account\CaseStudy\UpdateType',
            $caseStudyUpdate
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $lastUpdate = $caseStudy->getLatestUpdate();

            if ($lastUpdate->getHelpRequested() === true) {
                $caseStudyUpdate->setHelpRequested(true);

                $this->get('email')->sendEmailToAdminForCaseStudyHelp($lastUpdate);
            }

            $images = $request->request->get('images');

            if ($images !== null) {
                $imageRepository = $this->getDoctrine()->getRepository('AppBundle:Image');

                foreach ($images as $id) {
                    $image = $imageRepository->find($id);
                    $image->setCaseStudyUpdate($caseStudyUpdate);
                    $em->persist($image);
                }
            }

            $em->persist($caseStudy);
            $em->persist($caseStudyUpdate);
            $em->flush();

            $this->addFlash('success', 'Update saved.');

            return $this->redirectToRoute('case_study_single',
                array(
                    'slug' => $caseStudy->getSlug()
                )
            );
        }

        return $this->render('account/case_study/new_update.html.twig',
            array(
                'case_study' => $caseStudy,
                'case_study_update' => $caseStudyUpdate,
                'form' => $form->createView(),
            )
        );
    }
}