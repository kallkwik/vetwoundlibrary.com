<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vwl_static_page")
 */
class StaticPage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ORM\Column(type="string")
     */
    private $route;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $upperField;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lowerField;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="staticPage")
     */
    private $images;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return StaticPage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return StaticPage
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set route
     *
     * @param string $route
     *
     * @return StaticPage
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return StaticPage
     */
    public function addImage(\AppBundle\Entity\Image $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Image $image
     */
    public function removeImage(\AppBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set upperField
     *
     * @param string $upperField
     *
     * @return StaticPage
     */
    public function setUpperField($upperField)
    {
        $this->upperField = $upperField;

        return $this;
    }

    /**
     * Get upperField
     *
     * @return string
     */
    public function getUpperField()
    {
        return $this->upperField;
    }

    /**
     * Set lowerField
     *
     * @param string $lowerField
     *
     * @return StaticPage
     */
    public function setLowerField($lowerField)
    {
        $this->lowerField = $lowerField;

        return $this;
    }

    /**
     * Get lowerField
     *
     * @return string
     */
    public function getLowerField()
    {
        return $this->lowerField;
    }
}
