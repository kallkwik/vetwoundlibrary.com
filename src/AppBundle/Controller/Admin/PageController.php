<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/page")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PageController extends Controller
{
    /**
     * @Route("/", name="admin_page_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_page_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Page')->findAll();

        $paginator = $this->get('knp_paginator');
        $pages = $paginator->paginate($query, $page, Page::NUM_ITEMS);
        $pages->setUsedRoute('admin_page_index_paginated');

        return $this->render('admin/page/index.html.twig', array(
                'pages' => $pages
        ));
    }

    /**
     * @Route("/show/{slug}", name="admin_page_show")
     * @Route("/new", name="admin_page_new")
     */
    public function newShowAction(Page $page = null, Request $request)
    {
        if (isset($page) === false) {
            $page = new Page();
        } else {
            $deleteForm = $this->createDeleteForm($page);
            $renderParams['delete_form'] = $deleteForm->createView();
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\Page\PageType',
            $page
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Set slug to generated from title if no slug was entered
            $slug = $page->getSlug() === null ? $page->getTitle()
                : $page->getSlug();
            $page->setSlug(
                $this->get('slugger')->slugify($slug)
            );

            // Set SEO Title to title if no SEO title was entered.
            if ($page->getSeoTitle() === null) {
                $page->setSeoTitle($page->getTitle());
            }

            $images = $request->request->get('images');

            if ($images !== null) {
                $imageRepository = $this->getDoctrine()->getRepository('AppBundle:Image');

                foreach ($images as $id) {
                    $image = $imageRepository->find($id);
                    $image->setPage($page);
                    $em->persist($image);
                }
            }

            $page->setUser($this->getUser());

            $em->persist($page);
            $em->flush();

            $this->addFlash('success', 'Page has been saved.');

            return $this->redirectToRoute('admin_page_show',
                array(
                    'slug' => $page->getSlug()
                )
            );
        }
        $renderParams['form'] = $form->createView();
        $renderParams['page'] = $page;

        return $this->render('admin/page/show.html.twig', $renderParams);
    }

    /**
     * @Route("/{id}", name="admin_page_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Page $page, Request $request)
    {
        $form = $this->createDeleteForm($page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($page);
            $em->flush();

            $this->addFlash('success', 'Page deleted.');
        }

        return $this->redirectToRoute('admin_page_index');
    }

    public function createDeleteForm(Page $page)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_page_delete',
                array('id' => $page->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Delete Page',
                    'attr' => array(
                        'class' => 'btn btn-outline'
                    )
                )
            )
            ->getForm()
        ;
    }

    /**
     * @Route("/remove-image/{id}", name="admin_delete_page_image")
     */
    public function deletePageImage(Page $page)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($page->getImage());
        $page->setImage(null);
        $em->flush();

        return new Response();
    }

    /**
     * @Route("/remove-icon-image/{id}", name="admin_delete_page_icon_image")
     */
    public function deleteIconImage(Page $page)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($page->getIconImage());
        $page->setIconImage(null);
        $em->flush();

        return new Response();
    }
}