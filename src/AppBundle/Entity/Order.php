<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 * @ORM\Table(name="vwl_order")
 */
class Order
{
    const NUM_ITEMS = 10;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $wholesalerName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $wholesalerAccountNumber;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="orders")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="Distributor")
     */
    private $distributor;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Order
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set product
     *
     * @param  $product
     *
     * @return Order
     */
    public function setProduct($product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set user
     *
     * @param  $user
     *
     * @return Order
     */
    public function setUser($user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set distributor
     *
     * @param  $distributor
     *
     * @return Order
     */
    public function setDistributor($distributor = null)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * Get distributor
     *
     * @return 
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Set wholesalerName
     *
     * @param string $wholesalerName
     *
     * @return Order
     */
    public function setWholesalerName($wholesalerName)
    {
        $this->wholesalerName = $wholesalerName;

        return $this;
    }

    /**
     * Get wholesalerName
     *
     * @return string
     */
    public function getWholesalerName()
    {
        return $this->wholesalerName;
    }

    /**
     * Set wholesalerAccountNumber
     *
     * @param string $wholesalerAccountNumber
     *
     * @return Order
     */
    public function setWholesalerAccountNumber($wholesalerAccountNumber)
    {
        $this->wholesalerAccountNumber = $wholesalerAccountNumber;

        return $this;
    }

    /**
     * Get wholesalerAccountNumber
     *
     * @return string
     */
    public function getWholesalerAccountNumber()
    {
        return $this->wholesalerAccountNumber;
    }
}
