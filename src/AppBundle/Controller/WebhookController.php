<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/payment-webhooks")
 * @Method("POST")
 */
class WebhookController extends Controller
{
    /**
     * When customers card is declined.
     *
     * @Route("/charge-failed", name="you_should_know_my_name")
     *
     */
    public function chargeFailed(Request $request)
    {
        $event = json_decode($request->getContent());

        $doctrine = $this->getDoctrine();

        $subscription = $doctrine->getRepository('AppBundle:Customer')->findOneBy(
            array('stripeId' => $event->data->object->customer)
        )->getActiveSubscription();

        if ($subscription === null) {
            return $this->createNotFoundException();
        }

        $subscription->setStatus('canceled');

        $em = $doctrine->getEntityManager();
        $em->persist($subscription);
        $em->flush();

        $emailService = $this->get('email');
        $emailService->setUser($subscription->getUser());

        $emailService->sendEmailToUserSubscriptionCancelled();

        return new JsonResponse();
    }

    /**
     * When customer has been refunded from a dispute.
     *
     * @Route("/dispute-funds-withdrawn", name="dispute_funds_withdrawn")
     */
    public function chargeDisputeFundsWithdrawn(Request $request)
    {
        $event = json_decode($request->getContent());

        $doctrine = $this->getDoctrine();

        $subscription = $doctrine->getRepository('AppBundle:Customer')->findOneBy(
            array('stripeId' => $event->data->object->customer)
        )->getActiveSubscription();

        if ($subscription === null) {
            return $this->createNotFoundException();
        }

        $subscription->setStatus('canceled');

        $em = $doctrine->getEntityManager();
        $em->persist($subscription);
        $em->flush();

        $emailService = $this->get('email');
        $emailService->setUser($subscription->getUser());

        $emailService->sendEmailToUserSubscriptionCancelled();
        $emailService->sendEmailToAdminsDisputeFundsWithdrawn();

        return new JsonResponse();
    }
}