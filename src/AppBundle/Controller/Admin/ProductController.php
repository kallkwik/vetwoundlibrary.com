<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Image;
use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/product")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="admin_product_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_product_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Product')->findAll();

        $paginator = $this->get('knp_paginator');
        $products = $paginator->paginate($query, $page, Product::NUM_ITEMS);
        $products->setUsedRoute('admin_contact_index_paginated');

        return $this->render('admin/product/index.html.twig',
            array(
                'products' => $products
            )
        );
    }

    /**
     * @Route("/show/{slug}", name="admin_product_show")
     * @Route("/new", name="admin_product_new")
     */
    public function newShowAction(Product $product = null, Request $request)
    {
        if (isset($product) === false) {
            $product = new Product();
        } else {
            $deleteForm = $this->createDeleteForm($product);
            $renderParams['delete_form'] = $deleteForm->createView();
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\Product\ProductType',
            $product
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Set slug to generated from title if no slug was entered
            $slug = $product->getSlug() === null ? $product->getProductName()
                : $product->getSlug();
            $product->setSlug(
                $this->get('slugger')->slugify($slug)
            );

            // Set SEO Title to title if no SEO title was entered.
            if ($product->getSeoTitle() === null) {
                $product->setSeoTitle($product->getProductName());
            }

            $images = $request->request->get('images');

            if ($images !== null) {
                $imageRepository = $this->getDoctrine()->getRepository('AppBundle:Image');

                foreach ($images as $id) {
                    $image = $imageRepository->find($id);
                    $image->setProduct($product);
                    $em->persist($image);
                }
            }

            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'Product has been saved.');

            return $this->redirectToRoute('admin_product_show',
                array(
                    'slug' => $product->getSlug()
                )
            );
        }
        $renderParams['form'] = $form->createView();
        $renderParams['product'] = $product;

        if ($form->getData() !== null)
        {
            $renderParams['form_data'] = $form->getData();
        }
        return $this->render('admin/product/show.html.twig', $renderParams);
    }

    /**
     * @Route("/delete/{id}", name="admin_product_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Product $product, Request $request)
    {
        if (!$product->getOrders()->isEmpty()) {
            $this->addFlash('error', 'Product has been ordered and can not be deleted');

            return $this->redirectToRoute('admin_product_show', array(
                'slug' => $product->getSlug(),
            ));
        }

        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($product);
            $em->flush();

            $this->addFlash('success', 'Product Successfully Deleted.');
        }

        return $this->redirectToRoute('admin_product_index');
    }

    public function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_product_delete',
                array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Delete Product',
                    'attr' => array(
                        'class' => 'btn btn-outline'
                    )
                )
            )
            ->getForm()
        ;
    }

    /**
     * @Route("/remove-image/{id}", name="admin_delete_product_image")
     */
    public function deleteProductImage(Product $product)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($product->getFeaturedImage());
        $product->setFeaturedImage(null);
        $em->flush();

        return new Response();
    }
}