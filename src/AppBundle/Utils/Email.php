<?php

namespace AppBundle\Utils;

use Doctrine\ORM\EntityManager;
use Swift_Mailer;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Class Email
 * @package AppBundle\Utils
 *
 * EmailParameters Array
 *  [to]        array - set email address or addresses to send to
 *  [subject]   string - subject of email
 *  [body]      twig view - set view
 */
class Email
{
    protected $mailer;
    protected $message;
    protected $fromAddress;
    protected $twigEngine;
    protected $userRepository;
    protected $user;

    public function __construct(Swift_Mailer $mailer, TwigEngine $twigEngine, EntityManager $entityManager, $fromAddress)
    {
        $this->mailer = $mailer;
        $this->message = \Swift_Message::newInstance();
        $this->fromAddress = $fromAddress;
        $this->twigEngine = $twigEngine;
        $this->userRepository = $entityManager->getRepository('AppBundle:User');
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * USER EMAILS
     */

    public function sendEmailToUserChargeFailed()
    {
        return $this->sendEmail(
            array(
                'to' => array($this->user->getEmail()),
                'subject' => 'Your payment method has failed',
                'template' => 'email/user_charge_failed.html.twig',
            ),
            array(
                'user' => $this->user,
            )
        );
    }

    public function sendEmailToUserSubscriptionCancelled()
    {
        return $this->sendEmail(
            array(
                'to' => array ($this->user->getEmail()),
                'subject' => 'Your subscription has been cancelled',
                'template' => 'email/user_subscription_cancelled.html.twig',
            ),
            array(
                'user' => $this->user,
            )
        );
    }

    /**
     * ADMIN EMAILS
     */

    public function sendEmailToAdminsDisputeFundsWithdrawn()
    {
        return $this->sendEmail(
            array(
                'to' => $this->userRepository->findAllAdminEmails(),
                'subject' => 'Payment dispute',
                'template' => 'email/admin_dispute_funds_withdrawn.html.twig',
            ),
            array(
                'user' => $this->user,
            )
        );
    }

    public function sendEmailToAdminForCaseStudyHelp($lastUpdate)
    {
        return $this->sendEmail(
            array(
                'to' => $this->userRepository->findAllAdminEmails(),
                'subject' => 'Help Requested: ' . $lastUpdate->getCaseStudy()->getInjuryName(),
                'template' => 'email/admin_case_study_help_requested.html.twig',
            ),
            array(
                'user' => $lastUpdate->getUser(),
                'case_study' => $lastUpdate->getCaseStudy(),
                'last_update' => $lastUpdate,
            )
        );
    }

    public function sendEmailToAdminForContact($contact)
    {
        return $this->sendEmail(
            array(
                'to' => $this->userRepository->findAllAdminEmails(),
                'subject' => 'New Contact from vetwoundlibrary.com',
                'template' => 'email/contact.html.twig',
            ),
            array(
                'contact' => $contact,
            )
        );
    }

    /**
     * @param $values
     * [to]         array of emails
     * [subject]    subject text
     * [template]   twig template path
     * @param null $params
     * @return array
     * @throws \Twig_Error
     */
    protected function sendEmail($values, $params = null)
    {
        $this->message
            ->setSubject($values['subject'])
            ->setFrom($this->fromAddress)
            ->setBody(
                $this->twigEngine->render(
                    $values['template'],
                    $params
                ),
                'text/html'
            )
        ;

        $result = array();

        foreach ($values['to'] as $recipient) {
            $this->message->setTo($recipient);
            $result[] = $this->mailer->send($this->message);
        }

        return $result;
    }
}