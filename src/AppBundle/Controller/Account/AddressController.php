<?php

namespace AppBundle\Controller\Account;

use AppBundle\Entity\Address;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/account/address")
 */
class AddressController extends Controller
{
    /**
     * @Route("/delete/{id}", name="delete_address")
     */
    public function deleteAddress($id, Request $request)
    {
        $address = $this->getDoctrine()->getRepository('AppBundle:Address')->find($id);

        /*if ($address !== null && $address->getUser() === $this->getUser()) {

            $em = $this->getDoctrine()->getManager();

            $em->remove($address);
            $em->flush();

            $this->addFlash('success', 'Address has been successfully deleted');
        }*/

        $callingUrl = $request->headers->get('referer');
        $route = $this->get('router')->match($callingUrl);

        return new RedirectResponse($callingUrl);
    }
}