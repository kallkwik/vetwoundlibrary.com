<?php

namespace AppBundle\Utils;

class Stripe
{
    private $privateKey;

    public function __construct($stripePrivateKey)
    {
        $this->privateKey = $stripePrivateKey;
    }

    public function prepare()
    {
        $stripe = new \Stripe\Stripe;
        $stripe->setApiKey($this->privateKey);
    }
}