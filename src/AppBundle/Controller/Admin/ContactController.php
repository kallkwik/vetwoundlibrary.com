<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Contact;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/admin/contact")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ContactController extends Controller
{
    /**
     * @Route("/", name="admin_contact_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_contact_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     * @Security("has_role('ROLE_EXPERT') or has_role('ROLE_ANGEL') or has_role('ROLE_ADMIN')")
     */
    public function indexAction($page)
    {
        $authChecker = $this->get('security.authorization_checker');

        if($authChecker->isGranted('ROLE_ADMIN')) {
            $query = $this->getDoctrine()
                ->getRepository('AppBundle:Contact')->findAll();
        } else {
            $query = $this->getDoctrine()
                ->getRepository('AppBundle:Contact')->findBy(array(
                        'assigned' => $this->getUser()
                    )
                );
        }

        $paginator = $this->get('knp_paginator');
        $contacts = $paginator->paginate($query, $page, Contact::NUM_ITEMS);
        $contacts->setUsedRoute('admin_contact_index_paginated');

        return $this->render('admin/contact/index.html.twig',
            array(
                'contacts' => $contacts
            )
        );
    }

    /**
     * @Route("/show/{id}", name="admin_contact_show")
     * @Security("has_role('ROLE_EXPERT') or has_role('ROLE_ANGEL') or has_role('ROLE_ADMIN')")
     */
    public function showAction(Contact $contact, Request $request)
    {
        $form = $this->createForm(
            'AppBundle\Form\Admin\Contact\ContactType',
            $contact
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($contact);
            $em->flush();

            $this->addFlash('success', 'Contact successfully updated');

            return $this->redirectToRoute('admin_contact_show', array(
                'id' => $contact->getId()
            ));
        }

        $renderParams['form'] = $form->createView();
        $renderParams['contact'] = $contact;

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $renderParams['delete_form'] = $this->createDeleteForm($contact)->createView();
        }

        return $this->render('admin/contact/show.html.twig', $renderParams);
    }

    /**
     * @Route("/{id}", name="admin_contact_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Contact $contact, Request $request)
    {
        $form = $this->createDeleteForm($contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contact);
            $em->flush();

            $this->addFlash('success', 'Email Successfully Deleted.');
        }

        return $this->redirectToRoute('admin_contact_index');
    }

    public function createDeleteForm(Contact $contact)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_contact_delete',
                array('id' => $contact->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Delete Email',
                    'attr' => array(
                        'class' => 'btn btn-outline'
                    )
                )
            )
            ->getForm()
        ;
    }
}