<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CaseStudy;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/case-study")
 */
class CaseStudyController extends Controller
{
    /**
     * @Route("/", name="public_case_study_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="public_case_study_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs
            ->addItem('Home', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('Case Study');

        if ($this->get('security.authorization_checker')
                ->isGranted('ROLE_SUBSCRIBED_USER')
            || $this->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
            || $this->get('security.authorization_checker')
                ->isGranted('ROLE_EXPERT')
            || $this->get('security.authorization_checker')
                ->isGranted('ROLE_ANGEL')
        ) {
            $query = $this->getDoctrine()
                ->getRepository('AppBundle:CaseStudy')->findBy(
                    array('published' => array(
                        CaseStudy::PUBLIC_VISIBLE,
                        CaseStudy::SUBSCRIBED_VISIBLE
                    ))
                )
            ;
        } else {
            $query = $this->getDoctrine()
                ->getRepository('AppBundle:CaseStudy')->findBy(
                    array('published' => CaseStudy::PUBLIC_VISIBLE)
                )
            ;
        }

        $paginator = $this->get('knp_paginator');
        $caseStudies = $paginator->paginate($query, $page, CaseStudy::NUM_ITEMS);
        $caseStudies->setUsedRoute('public_case_study_index_paginated');

        return $this->render('case_study/index.html.twig',
            array('case_studies' => $caseStudies)
        );
    }

    /**
     * @Route("/view/{slug}", name="public_case_study_single")
     */
    public function showAction(CaseStudy $caseStudy)
    {
        if ($caseStudy->getPublished() !== false) {
            $response = $this->render('case_study/show.html.twig',
                array('case_study' => $caseStudy)
            );
        } else {
            $response = $this->render('case_study/show.html.twig',
                array(
                    'error' => 'Case Study not found.',
                    'case_study' => new CaseStudy()
                )
            );
        }

        return $response;
    }

    public function uploadImageAction(CaseStudy $caseStudy)
    {

    }
}