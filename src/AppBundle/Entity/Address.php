<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vwl_address")
 */
class Address
{
    const BILLING = 1;
    const SHIPPING = 2;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstLine;

    /**
     * @ORM\Column(type="string")
     */
    private $secondLine;

    /**
     * @ORM\Column(type="string")
     */
    private $town;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string")
     */
    private $postcode;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="addresses")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstLine
     *
     * @param string $firstLine
     *
     * @return Address
     */
    public function setFirstLine($firstLine)
    {
        $this->firstLine = $firstLine;

        return $this;
    }

    /**
     * Get firstLine
     *
     * @return string
     */
    public function getFirstLine()
    {
        return $this->firstLine;
    }

    /**
     * Set secondLine
     *
     * @param string $secondLine
     *
     * @return Address
     */
    public function setSecondLine($secondLine)
    {
        $this->secondLine = $secondLine;

        return $this;
    }

    /**
     * Get secondLine
     *
     * @return string
     */
    public function getSecondLine()
    {
        return $this->secondLine;
    }

    /**
     * Set town
     *
     * @param string $town
     *
     * @return Address
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set county
     *
     * @param string $county
     *
     * @return Address
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Address
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Address
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
