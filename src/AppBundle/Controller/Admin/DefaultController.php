<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\CaseStudy;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_EXPERT') or has_role('ROLE_ANGEL') or has_role('ROLE_ADMIN')")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     */
    public function indexAction()
    {
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_index'));

        $caseStudies = $this->isGranted('ROLE_ADMIN')
            ? $this->getDoctrine()->getRepository('AppBundle:CaseStudy')
                ->findByUnassignedHelpRequested()
            : null;

        $assignedEmails = $this->getDoctrine()->getRepository('AppBundle:Contact')->findAll();
        $orders = $this->getDoctrine()->getRepository('AppBundle:Order')
            ->findOrdersAfterDate((new \DateTime())->modify('-1 month'));

        return $this->render('admin/default/index.html.twig', array(
            'case_studies_help' => $caseStudies,
            'assigned_emails' => $assignedEmails,
            'orders' => $orders
        ));
    }
}