<?php

namespace AppBundle\Form\Admin\CaseStudy;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, array(
                'attr' => array('class' =>'form-control')
            ))
            ->add('woundSize', TextType::class, array(
                'attr' => array('class' =>'form-control'),
                'required' => false
            ))
            ->add('tissueStatus',  TextType::class, array(
                'attr' => array('class' =>'form-control'),
                'required' => false
            ))
            ->add('user', EntityType::class, array(
                'attr' => array('class' => 'form-control'),
                'class' => 'AppBundle:User',
                'choice_label' => function ($user) {
                    return $user->getFirstName() . ' ' . $user->getLastName()
                        . ' - ' . $user->getEmail();
                }
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary btn-block'
                ),
                'label' => 'Save Case Study Update'
            ))
        ;
    }
}