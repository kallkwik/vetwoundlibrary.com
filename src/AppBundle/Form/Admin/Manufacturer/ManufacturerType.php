<?php

namespace AppBundle\Form\Admin\Manufacturer;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ManufacturerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array('class' => 'form-control'),
            ))
            ->add('description', TextareaType::class, array(
                'attr' => array('class' => 'form-control'),
            ))
            ->add('listProducts', CheckboxType::class, array(
                'attr' => array('class' => 'checkbox'),
                'label' => 'List products from this manufacturer?',
                'required' => false,
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-block btn-primary'),
                'label' => 'Save Manufacturer',
            ))
        ;
    }
}