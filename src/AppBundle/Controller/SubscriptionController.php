<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\Plan;
use AppBundle\Entity\Subscription;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/subscription")
 */
class SubscriptionController extends Controller
{
    /**
     * @Route("/new/{id}", name="subscription_new")
     */
    public function newSubscription(Plan $plan, Request $request)
    {
        $form = $this->createForm('AppBundle\Form\SubscriptionType');
        $form->handleRequest($request);
        echo $form->getErrors(true);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();


            $subscription = $this->processSubscription(
                $plan, $this->getUser(), $formData
            );

            if ($subscription === true) {
                return $this->redirectToRoute('account_index');
            }
        }

        return $this->render('subscription/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function prepareStripe()
    {
        $apiKey = $this->getParameter('stripe_private');
        $stripe = new \Stripe\Stripe;
        $stripe->setApiKey($apiKey);
    }

    /**
     * Will check if customer exists in Stripe, if not will create customer.
     *
     * Returns Stripe Customer object.
     */
    public function createStripeCustomer(User $user)
    {
        $this->prepareStripe();

        $stripeCustomer = \Stripe\Customer::create(
            array(
                'metadata' => array(
                    'name' => $user->getFirstName() . ' ' . $user->getLastName(),
                    'id' => $this->getUser()->getId(),
                ),
            )
        );

        $em = $this->getDoctrine()->getManager();

        $user->setStripeId($stripeCustomer['id']);

        $em->persist($user);
        $em->flush();

        return $stripeCustomer;
    }

    public function processSubscription(Plan $plan, User $user, $formData)
    {
        $em = $this->getDoctrine()->getManager();

        $this->prepareStripe();

        if ($user->getStripeId() === null) {
            $customer = $this->createStripeCustomer($user);
        } else {
            $customer = \Stripe\Customer::retrieve($user->getStripeId());
        }

        /**
         * Check if address is selected from existing addresses, if a new address
         * has been entered or if no address has been entered at all.
         */
        if ($formData['address'] !== null) {
            $address = $this->resolveAddressEntityToStripeAddressObject($formData['address']);
        } elseif ($this->resolveAddressFormDataToStripeAddressObject($formData) !== null) {
            $em->persist($this->resolveAddressFormDataToEntity($formData));

            $address = $this->resolveAddressFormDataToStripeAddressObject($formData);
        } else {
            $this->addFlash('warning', 'Please select existing address or enter a new address below.');
            return false;
        }

        $cardDetails = $this->resolveFormCardDetailsToStripeObject($formData);

        $subArgs = array(
            'plan' => $plan->getId(),
            'source' => array_merge($cardDetails, $address),
        );

        if ($user->getActiveSubscription() === null) {
            try {
                $stripeSub = $customer->subscriptions->create($subArgs);

                $subscription = new Subscription();
                $subscription->setPlan($plan);
                $subscription->setStripeId($stripeSub['id']);
                $subscription->setUser($user);
                $subscription->setStatus('active');

                $subscription->setPeriodStart(
                    new \DateTime(date('Y-m-d H:i:s', $stripeSub['current_period_start']))
                );
                $subscription->setPeriodEnd(
                    new \DateTime(date('Y-m-d H:i:s', $stripeSub['current_period_end']))
                );

                $user->setActiveSubscription($subscription);
                $user->setRole(array('ROLE_SUBSCRIBED_USER'));

                $em->persist($user);
                $em->persist($subscription);

                $em->flush();

                $this->addFlash('success', 'Subscription successfully updated.');
                return true;
            } catch(\Stripe\Error\Card $e) {
                $body = $e->getJsonBody();
                $err = $body['error'];

                $this->addFlash($err['message'], $e);
            } catch (\Stripe\Error\RateLimit $e) {
                $this->addFlash('warning', $e);
            } catch (\Stripe\Error\InvalidRequest $e) {
                $this->addFlash('warning', $e);
            } catch (\Stripe\Error\Authentication $e) {
                $this->addFlash('warning', $e);
            } catch (\Stripe\Error\ApiConnection $e) {
                $this->addFlash('warning', $e);
            } catch (\Stripe\Error\Base $e) {
                $this->addFlash('warning', $e);
            } catch (\Exception $e) {
                $this->addFlash('warning', 'An unexpected error has occured. Please contact us.' . $e);
            }
        } else {
            $this->addFlash('warning', 'Subscription is already active on this account.');
        }

        return false;
    }

    public function resolveAddressFormDataToEntity($formData)
    {
        $address = new Address();

        if (
            $formData['firstLine'] === null
            || $formData['secondLine'] === null
            || $formData['town'] === null
            || $formData['county'] === null
            || $formData['postcode'] === null
        ) {
            return null;
        }

        $address->setFirstLine($formData['firstLine']);
        $address->setSecondLine($formData['secondLine']);
        $address->setTown($formData['town']);
        $address->setCounty($formData['county']);
        $address->setPostcode($formData['postcode']);
        $address->setUser($this->getUser());

        return $address;
    }

    public function resolveFormCardDetailsToStripeObject($formData)
    {
        return array(
            'number' => $formData['cardNumber'],
            'exp_month' => date('d', $formData['cardExpDate']->getTimestamp()),
            'exp_year' =>  date('Y', $formData['cardExpDate']->getTimestamp()),
            'object' => 'card',
            'cvc' => $formData['cvc'],
            'name' => $formData['cardHolderName'],
        );
    }

    public function resolveAddressEntityToStripeAddressObject(Address $address)
    {
        return array(
            'address_country' => 'United Kingdom',
            'address_line1' => $address->getFirstLine(),
            'address_line2' => $address->getSecondLine(),
            'address_state' => $address->getCounty(),
            'address_zip' => $address->getPostcode()
        );
    }

    public function resolveAddressFormDataToStripeAddressObject($formData)
    {
        return array(
            'address_country' => 'United Kingdom',
            'address_line1' => $formData['firstLine'],
            'address_line2' => $formData['secondLine'],
            'address_state' => $formData['county'],
            'address_zip' => $formData['postcode'],
        );
    }
}