<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/")
 */
class UserController extends Controller
{
    /**
     * @Route("/angels", name="index_angel")
     */
    public function indexAngelAction()
    {
        $doctrine = $this->getDoctrine();

        $angels = $doctrine->getRepository('AppBundle:User')
            ->findByRole('ROLE_ANGEL', true);

        $staticPage = $doctrine->getRepository('AppBundle:StaticPage')
            ->findOneBy(array(
                'route' => 'index_angel'
            ));

        return $this->render('user/index_angel.html.twig', array(
            'angels' => $angels,
            'page' => $staticPage,
        ));
    }

    /**
     * @Route("/experts", name="index_expert")
     */
    public function indexExpertAction()
    {
        $doctrine = $this->getDoctrine();

        $experts = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findByRole('ROLE_EXPERT', true);

        $staticPage = $doctrine->getRepository('AppBundle:StaticPage')
            ->findOneBy(array(
                'route' => 'index_angel'
            ));

        return $this->render('user/index_expert.html.twig', array(
            'experts' => $experts,
            'page' => $staticPage,
        ));
    }

    /**
     * @Route("/angels/{id}", name="show_angel")
     * @Route("/experts/{id}", name="show_expert")
     */
    public function showAction(User $user)
    {
        if (count(array_intersect($user->getRoles(), array('ROLE_ANGEL', 'ROLE_EXPERT'))) === 0) {
            throw $this->createNotFoundException('User not found.');
        }

        return $this->render('user/show.html.twig', array(
            'user' => $user
        ));
    }
}