<?php

namespace AppBundle\Form\Admin\Product;

use AppBundle\Form\Type\ImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productName', TextType::class,
                array(
                    'attr' => array('class' => 'form-control')
                )
            )
            ->add('genericName', TextType::class,
                array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                )
            )
            ->add('description', TextareaType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control tinymce',
                        'data-theme' => 'advanced',
                        'rows' => '12'
                    )
                )
            )
            ->add('seoTitle', TextType::class,
                array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                )
            )
            ->add('metaDescription', TextareaType::class,
                array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                )
            )
            ->add('slug', TextType::class,
                array(
                    'attr' => array('class' =>'form-control'),
                    'required' => false
                )
            )
            ->add('published', ChoiceType::class,
                array(
                    'label' => 'Status',
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'Published' => true,
                        'Unpublished' => false,
                    ),
                )
            )
            ->add('category', EntityType::class,
                array(
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'name',
                    'attr' => array('class' => 'form-control'),
                    'required' => false,
                    'placeholder' => 'Select a Category'
                )
            )
            ->add('manufacturer', EntityType::class,
                array(
                    'class' => 'AppBundle:Manufacturer',
                    'choice_label' => 'name',
                    'attr' => array('class' => 'form-control'),
                    'required' => true,
                    'placeholder' => 'Select a Manufacturer'
                )
            )
            ->add('distributors', EntityType::class,
                array(
                    'class' => 'AppBundle:Distributor',
                    'choice_label' => 'name',
                    'attr' => array('class' => 'form-control'),
                    'required' => false,
                    'placeholder' => 'Select a Distributor',
                    'multiple' => true,
                    'expanded' => true,
                )
            )
            ->add('price', MoneyType::class,
                array(
                    'currency' => 'gbp',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                )
            )
            ->add('instructions', TextareaType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control tinymce',
                        'data-theme' => 'advanced',
                        'rows' => '12'
                    )
                )
            )
            ->add('featuredImage', ImageType::class, array('required' => false))
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Save Product',
                    'attr' => array(
                        'class' => 'btn btn-block btn-primary'
                    )
                )
            )
        ;
    }
}