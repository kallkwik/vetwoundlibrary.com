<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\StaticPage;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadStaticPageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->generateStaticPage('Home', 'home', 'homepage', 'home-page', $manager);
        $this->generateStaticPage('Expert List', 'experts', 'index_angel', 'angel-index-page', $manager);
        $this->generateStaticPage('Angel List', 'angels', 'index_expert', 'expert-index-page', $manager);
    }

    public function getOrder()
    {
        return 1;
    }

    private function generateStaticPage($name, $slug, $route, $reference, $manager)
    {
        $staticPage = new StaticPage();

        $staticPage->setName($name);
        $staticPage->setSlug($slug);
        $staticPage->setRoute($route);

        $manager->persist($staticPage);
        $manager->flush();

        $this->addReference($reference, $staticPage);
    }
}