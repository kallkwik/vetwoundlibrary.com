<?php

namespace AppBundle\Form\Admin\CaseStudy;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaseStudyNewEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('injuryName', TextType::class, array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('petBreed', TextType::class, array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('category', ChoiceType::class, array(
                'choices' => array(
                    'Please select Animal Type' => null,
                    'Small' => 1,
                    'Large' => 2,
                    'Exotic' => 3,
                ),
                'choices_as_values' => true,
                'attr' => array('class' => 'form-control')
            ))
            ->add('petBirthDate', DateType::class, array(
                'widget' => 'single_text',
                'attr' => array('class' => 'form-control')
            ))
            ->add('petSex', ChoiceType::class, array(
                'choices' => array(
                    'Please select pet gender' => null,
                    'Male' => true,
                    'Female' => false,
                ),
                'attr' => array('class' => 'form-control')
            ))
            ->add('tried', TextareaType::class, array(
                'required' => false,
                'attr' => array('class' => 'form-control')
            ))
            ->add('woundLocation', TextType::class, array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('woundDate', DateType::class, array(
                'widget' => 'single_text',
                'attr' => array('class' => 'form-control')
            ))
            ->add('user', EntityType::class, array(
                'class' => 'AppBundle:User',
                'attr' => array('class' => 'form-control'),
                'choice_label' => function ($user) {
                    return $user->getFirstName() . ' ' . $user->getLastName();
                },
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Save Case Study',
                'attr' => array('class' => 'btn btn-block btn-primary')
            ))
        ;
    }
}