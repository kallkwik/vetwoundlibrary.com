<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PageController extends Controller
{
    /**
     * @Route("/page/{slug}", name="page_show")
     */
    public function indexAction(Page $page)
    {
        if ($page->getPublished() === false) {
            throw $this->createNotFoundException();
        }

        return $this->render('default/page.html.twig',
            array('page' => $page)
        );
    }
}