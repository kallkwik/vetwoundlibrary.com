<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/news")
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="post_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="post_index_paginated",
     *     requirements={"page" : "\d+"})
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Post')->findBy(
                array('published' => true),
                array('dateSubmitted' => 'DESC')
            );
        $paginator = $this->get('knp_paginator');
        $posts = $paginator->paginate($query, $page, Post::NUM_ITEMS);
        $posts->setUsedRoute('post_index_paginated');

        return $this->render('post/index.html.twig',
            array('posts' => $posts)
        );
    }

    /**
     * @Route("/view/{slug}", name="post_show")
     */
    public function showAction(Post $post)
    {
        if ($post->getPublished() !== true) {
            throw $this->createNotFoundException();
        }

        return $this->render('post/show.html.twig',
            array('post' => $post)
        );
    }
}