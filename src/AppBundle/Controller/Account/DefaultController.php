<?php

namespace AppBundle\Controller\Account;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/account")
 * @Security("has_role('ROLE_USER') or has_role('ROLE_SUBSCRIBED_USER')")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="account_index")
     */
    public function indexAction()
    {
        $authChecker = $this->get('security.authorization_checker');
        if ($authChecker->isGranted('ROLE_ADMIN') ||
            $authChecker->isGranted('ROLE_EXPERT') ||
            $authChecker->isGranted('ROLE_ANGEL')
        ) {
            return $this->redirectToRoute('admin_index');
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs
            ->addItem('Home', $this->get('router')->generate('homepage'));
        $breadcrumbs->addItem('Account');

        return $this->render('account/index.html.twig');
    }
}