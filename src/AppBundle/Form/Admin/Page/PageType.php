<?php

namespace AppBundle\Form\Admin\Page;

use AppBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
                array(
                    'attr' => array('class' => 'form-control')
                )
            )
            ->add('subtitle', TextType::class,
                array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                )
            )
            ->add('content', TextareaType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control tinymce',
                        'data-theme' => 'advanced',
                        'rows' => '12'
                    )
                )
            )
            ->add('seoTitle', TextType::class,
                array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                )
            )
            ->add('metaDescription', TextareaType::class,
                array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                )
            )
            ->add('slug', TextType::class,
                array(
                    'attr' => array('class' =>'form-control'),
                    'required' => false
                )
            )
            ->add('published', ChoiceType::class,
                array(
                    'label' => 'Status',
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'Published' => true,
                        'Unpublished' => false,
                    ),
                )
            )
            ->add('image', ImageType::class, array('required' => false))
            ->add('iconImage', ImageType::class, array('required' => false))
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Save Page',
                    'attr' => array(
                        'class' => 'btn btn-block btn-primary'
                    )
                )
            )
        ;
    }
}