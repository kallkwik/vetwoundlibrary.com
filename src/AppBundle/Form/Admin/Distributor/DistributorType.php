<?php

namespace AppBundle\Form\Admin\Distributor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class DistributorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array('class' => 'form-control'),
            ))
            ->add('description', TextareaType::class, array(
                'attr' => array('class' => 'form-control'),
                'required' => false
            ))
            ->add('contactFirstName', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'First Name'
            ))
            ->add('contactLastName', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Last Name'
            ))
            ->add('contactEmail', EmailType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Email'
            ))
            ->add('contactNumber', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Number'
            ))
            ->add('sponsored', CheckboxType::class, array(
                'attr' => array('class' => 'checkbox'),
                'label' => 'Is a paying sponsor?',
                'required' => false,
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-block btn-primary'),
                'label' => 'Save Distributor'
            ))
        ;
    }
}