<?php

namespace AppBundle\Controller\Account;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Route("/account/subscription")
 * @Security("has_role('ROLE_SUBSCRIBED_USER')")
 */
class SubscriptionController extends Controller
{
    private $user;
    private $em;
    private $customer;
    private $stripeSubsriptionObject;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->user = $this->getUser();
        $this->em = $this->getDoctrine()->getManager();

        $this->get('stripe')->prepare();


        if ($this->user->getActiveSubscription() !==  null
            && $this->user->getActiveSubscription()->getStatus() === 'active'
        ) {
            $this->customer = \Stripe\Customer::retrieve($this->getUser()->getStripeId());

            $this->stripeSubsriptionObject = $this->customer->subscriptions->retrieve(
                $this->getUser()->getActiveSubscription()->getStripeId()
            );
        }
    }

    /**
     * @Route("/cancel", name="account_subscription_cancel")
     * @Security("has_role('ROLE_SUBSCRIBED_USER')")
     *
     * This will set the subscription to cancel.
     *
     * An event listener is set that will check if a subscription is cancelled
     * and if the end date has passed and will remove subscription role and
     * status from the user.
     *
     */
    public function cancelAction()
    {
        $subscriptionEntity = $this->getUser()->getActiveSubscription();
        $end = date("ga:i, d/m/Y", $subscriptionEntity->getPeriodEnd()->getTimestamp());

        if ($subscriptionEntity->getStatus() == 'active') {
            $response = $this->stripeSubsriptionObject->cancel();

            $subscriptionEntity->setStatus('canceled');

            $this->em->persist($this->user);
            $this->em->flush();


            $this->addFlash('success',
                'Your subscription has been cancelled. It will expire on '
                . $end . '.'
            );

        } elseif ($subscriptionEntity->getStatus() == 'canceled') {
            $this->addFlash('success',
                'Your subscription has already been cancelled. It will expire on '
                . $end . '.'
            );
        }

        return $this->redirectToRoute('account_index');

    }
}
