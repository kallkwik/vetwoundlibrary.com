<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/payment")
 */
class PaymentController extends Controller
{
    /**
     * @Route("/success", name="payment_success")
     */
    public function successAction()
    {

    }

    /**
     * @Route("/failure", name="payment_failure")
     */
    public function failureAction()
    {

    }
}