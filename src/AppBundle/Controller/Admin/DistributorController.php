<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Distributor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/distributor")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DistributorController extends Controller
{
    /**
     * @Route("/", name="admin_distributor_index", defaults={"page" = 1})
     * @Route("/page/{page}", name="admin_distributor_index_paginated",
     *     requirements={"page" : "\d+"})
     * @Cache(smaxage="10")
     */
    public function indexAction($page)
    {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Distributor')->findAll();

        $paginator = $this->get('knp_paginator');
        $distributors = $paginator->paginate($query, $page,
            Distributor::NUM_ITEMS);
        $distributors->setUsedRoute('admin_distributor_index_paginated');

        return $this->render('admin/distributor/index.html.twig',
            array(
                'distributors' => $distributors
            )
        );
    }

    /**
     * @Route("/show/{id}", name="admin_distributor_show")
     * @Route("/new", name="admin_distributor_new")
     */
    public function newShowAction(
        Distributor $distributor = null,
        Request $request
    ) {
        if (isset($distributor) === false) {
            $distributor = new Distributor();
        }

        $form = $this->createForm(
            'AppBundle\Form\Admin\Distributor\DistributorType',
            $distributor
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($distributor);
            $em->flush();

            $this->addFlash('success', 'Distributor has been saved.');

            return $this->redirectToRoute('admin_distributor_show',
                array(
                    'id' => $distributor->getId()
                )
            );
        }

        return $this->render('admin/distributor/show.html.twig',
            array(
                'distributor' => $distributor,
                'form' => $form->createView(),
                'delete_form' => $this->createDeleteForm($distributor)->createView(),
            )
        );
    }

    /**
     * @Route("/delete/{id}", name="admin_distributor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Distributor $distributor, Request $request)
    {
        if (!$distributor->getProducts()->isEmpty()) {
            $this->addFlash('error', 'Distributor has products and cannot be deleted');

            return $this->redirectToRoute('admin_distributor_show', array(
                'id' => $distributor->getId())
            );
        }

        $form = $this->createDeleteForm($distributor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($distributor);
            $em->flush();

            $this->addFlash('success', 'Distributor Successfully Deleted.');
        }

        return $this->redirectToRoute('admin_distributor_index');
    }

    public function createDeleteForm(Distributor $distributor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_distributor_delete',
                array('id' => $distributor->getId())))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class,
                array(
                    'label' => 'Delete Distributor',
                    'attr' => array(
                        'class' => 'btn btn-outline',
                    )
                )
            )
            ->getForm()
        ;
    }
}