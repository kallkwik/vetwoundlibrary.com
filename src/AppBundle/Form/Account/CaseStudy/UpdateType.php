<?php

namespace AppBundle\Form\Account\CaseStudy;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class)
            ->add('woundSize', TextType::class)
            ->add('tissueStatus', TextType::class)
            ->add('helpRequested', CheckboxType::class, array(
                'label' => 'Request help from our Angels/Experts'
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary'
                ),
                'label' => 'Save Case Study Update'
            ))
        ;
    }
}