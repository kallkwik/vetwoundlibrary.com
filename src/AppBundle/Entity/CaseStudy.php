<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CaseStudyRepository")
 * @ORM\Table(name="vwl_case_study")
 */
class CaseStudy
{
    const NUM_ITEMS = 10;
    const LARGE_ANIMALS = 1;
    const SMALL_ANIMALS = 2;
    const EXOTIC_ANIMALS = 3;

    const MALE = false;
    const FEMALE = true;

    const NOT_VISIBLE = 0;
    const SUBSCRIBED_VISIBLE = 1;
    const PUBLIC_VISIBLE = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="injury_name")
     */
    private $injuryName;

    /**
     * @ORM\Column(type="string", name="pet_breed")
     */
    private $petBreed;

    /**
     * @ORM\Column(type="datetime", name="pet_birth_date")
     */
    private $petBirthDate;

    /**
     * @ORM\Column(type="boolean", name="pet_sex")
     */
    private $petSex;

    /**
     * @ORM\Column(type="datetime", name="wound_date")
     */
    private $woundDate;

    /**
     * @ORM\Column(type="string", name="wound_location")
     */
    private $woundLocation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tried;

    /**
     * @ORM\Column(type="integer")
     */
    private $category;

    /**
     * @ORM\Column(type="datetime", name="date_submitted")
     */
    private $dateSubmitted;

    /**
     * @ORM\OneToOne(targetEntity="CaseStudyUpdate")
     */
    private $latestUpdate;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean", name="is_archived")
     */
    private $archived = false;

    /**
     * @ORM\Column(type="integer", name="is_published")
     */
    private $published = 0;

    /**
     * @ORM\OneToMany(targetEntity="CaseStudyUpdate", mappedBy="caseStudy")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $caseStudyUpdate;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="Image", cascade={"persist"})
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="assignedCaseStudies")
     */
    private $assignedUser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->caseStudyUpdate = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set injuryName
     *
     * @param string $injuryName
     *
     * @return CaseStudy
     */
    public function setInjuryName($injuryName)
    {
        $this->injuryName = $injuryName;

        return $this;
    }

    /**
     * Get injuryName
     *
     * @return string
     */
    public function getInjuryName()
    {
        return $this->injuryName;
    }

    /**
     * Set petBreed
     *
     * @param string $petBreed
     *
     * @return CaseStudy
     */
    public function setPetBreed($petBreed)
    {
        $this->petBreed = $petBreed;

        return $this;
    }

    /**
     * Get petBreed
     *
     * @return string
     */
    public function getPetBreed()
    {
        return $this->petBreed;
    }

    /**
     * Set petSex
     *
     * @param integer $petSex
     *
     * @return CaseStudy
     */
    public function setPetSex($petSex)
    {
        $this->petSex = $petSex;

        return $this;
    }

    /**
     * Get petSex
     *
     * @return integer
     */
    public function getPetSex()
    {
        return $this->petSex;
    }

    /**
     * Set woundLocation
     *
     * @param string $woundLocation
     *
     * @return CaseStudy
     */
    public function setWoundLocation($woundLocation)
    {
        $this->woundLocation = $woundLocation;

        return $this;
    }

    /**
     * Get woundLocation
     *
     * @return string
     */
    public function getWoundLocation()
    {
        return $this->woundLocation;
    }

    /**
     * Set category
     *
     * @param integer $category
     *
     * @return CaseStudy
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CaseStudy
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     *
     * @return CaseStudy
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return CaseStudy
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Add caseStudyUpdate
     *
     * @param \AppBundle\Entity\CaseStudyUpdate $caseStudyUpdate
     *
     * @return CaseStudy
     */
    public function addCaseStudyUpdate(\AppBundle\Entity\CaseStudyUpdate $caseStudyUpdate)
    {
        $this->caseStudyUpdate[] = $caseStudyUpdate;

        return $this;
    }

    /**
     * Remove caseStudyUpdate
     *
     * @param \AppBundle\Entity\CaseStudyUpdate $caseStudyUpdate
     */
    public function removeCaseStudyUpdate(\AppBundle\Entity\CaseStudyUpdate $caseStudyUpdate)
    {
        $this->caseStudyUpdate->removeElement($caseStudyUpdate);
    }

    /**
     * Get caseStudyUpdate
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCaseStudyUpdate()
    {
        return $this->caseStudyUpdate;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return CaseStudy
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set image
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return CaseStudy
     */
    public function setImage(\AppBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set petBirthDate
     *
     * @param \DateTime $petBirthDate
     *
     * @return CaseStudy
     */
    public function setPetBirthDate($petBirthDate)
    {
        $this->petBirthDate = $petBirthDate;

        return $this;
    }

    /**
     * Get petBirthDate
     *
     * @return \DateTime
     */
    public function getPetBirthDate()
    {
        return $this->petBirthDate;
    }

    /**
     * Set woundDate
     *
     * @param \DateTime $woundDate
     *
     * @return CaseStudy
     */
    public function setWoundDate($woundDate)
    {
        $this->woundDate = $woundDate;

        return $this;
    }

    /**
     * Get woundDate
     *
     * @return \DateTime
     */
    public function getWoundDate()
    {
        return $this->woundDate;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return CaseStudy
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set assignedUser
     *
     * @param \AppBundle\Entity\User $assignedUser
     *
     * @return CaseStudy
     */
    public function setAssignedUser(\AppBundle\Entity\User $assignedUser = null)
    {
        $this->assignedUser = $assignedUser;

        return $this;
    }

    /**
     * Get assignedUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getAssignedUser()
    {
        return $this->assignedUser;
    }

    /**
     * Set dateSubmitted
     *
     * @param \DateTime $dateSubmitted
     *
     * @return CaseStudy
     */
    public function setDateSubmitted($dateSubmitted)
    {
        $this->dateSubmitted = $dateSubmitted;

        return $this;
    }

    /**
     * Get dateSubmitted
     *
     * @return \DateTime
     */
    public function getDateSubmitted()
    {
        return $this->dateSubmitted;
    }

    /**
     * Set latestUpdate
     *
     * @param \AppBundle\Entity\CaseStudyUpdate $latestUpdate
     *
     * @return CaseStudy
     */
    public function setLatestUpdate(\AppBundle\Entity\CaseStudyUpdate $latestUpdate = null)
    {
        $this->latestUpdate = $latestUpdate;

        return $this;
    }

    /**
     * Get latestUpdate
     *
     * @return \AppBundle\Entity\CaseStudyUpdate
     */
    public function getLatestUpdate()
    {
        return $this->latestUpdate;
    }

    /**
     * Set tried
     *
     * @param string $tried
     *
     * @return CaseStudy
     */
    public function setTried($tried)
    {
        $this->tried = $tried;

        return $this;
    }

    /**
     * Get tried
     *
     * @return string
     */
    public function getTried()
    {
        return $this->tried;
    }
}
